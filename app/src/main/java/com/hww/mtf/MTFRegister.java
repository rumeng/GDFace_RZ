package com.hww.mtf;

import android.content.Context;
import android.util.Log;

import com.hww.gdface_rz.util.SharePreferencesUtils;

import net.gdface.license.RegisterException;
import net.gdface.mtfsdk.FaceApiMtfAndroid;


public class MTFRegister {

    /**
     * 公司授权字段
     */
    public static final String LICENSE_KEY = "licenseKey";
    /**
     * 授权码验证字段
     */
    public static final String LICENSE_CODE = "licenseCode";

    /**
     * 授权状态
     */
    public static final String LICENSE_STATE = "LICENSE_STATE";


    public static MsgOnline online(Context context, String licenseKey) {
        MsgOnline msgOnline = new MsgOnline();
        boolean validStart = FaceApiMtfAndroid.licenseManager().isValidLicense();
        if (validStart) {
            msgOnline.setState(true);
            msgOnline.setMsg("本地已授权成功");
        } else {
            boolean online = SharePreferencesUtils.getBoolean(context, LICENSE_STATE);
            String shareValue = SharePreferencesUtils.getString(context, LICENSE_CODE);
            if (online) {
                msgOnline = onlineLicenseCode(context, licenseKey, shareValue);
            } else {
                if (licenseKey.length() > 0) {
                    try {
                        String license = FaceApiMtfAndroid.licenseManager().licenseOnline(licenseKey);
                        msgOnline = onlineLicenseCode(context, licenseKey, license);
                    } catch (RegisterException e) {
                        msgOnline.setState(false);
                        msgOnline.setMsg("Exception:" + e.toString());
                        e.printStackTrace();
                    }
                } else {
                    msgOnline.setState(false);
                    msgOnline.setMsg("未设置授权关键字");
                }
            }
            Log.i("hww", "mtfSDK online == " + msgOnline.toString());
        }
        return msgOnline;
    }

    public static MsgOnline onlineLicenseCode(Context context, String licenseKey, String licenseCode) {
        MsgOnline msgOnline = new MsgOnline();
        if (null != licenseKey && licenseKey.length() > 0 && null != licenseCode && licenseCode.length() > 0) {
            try {
                boolean install = FaceApiMtfAndroid.licenseManager().installLicense(licenseKey, licenseCode);
                if (install) {
                    boolean valid = FaceApiMtfAndroid.licenseManager().isValidLicense();
                    if (valid) {
                        msgOnline.setState(true);
                        msgOnline.setMsg("Key And Code授权成功");
                        SharePreferencesUtils.putString(context, LICENSE_KEY, licenseKey);
                        SharePreferencesUtils.putString(context, LICENSE_CODE, licenseCode);
                        SharePreferencesUtils.putBoolean(context, LICENSE_STATE, true);

//                        FaceApiMtfAndroid.getInstance().setGlobalRuntimeParam(MtfAndroidArmBridge.RuntimeParam.minFaceSize, 48);
//                        FaceApiMtfAndroid.getInstance().setGlobalRuntimeParam(MtfAndroidArmBridge.RuntimeParam.featureThreadNumber, 1);
                    } else {
                        msgOnline.setState(false);
                        msgOnline.setMsg("Key And Code授权失败");
                    }
                } else {
                    msgOnline.setState(false);
                    msgOnline.setMsg("installLicense false");
                }
            } catch (Exception e) {
                e.printStackTrace();
                msgOnline.setState(false);
                msgOnline.setMsg(e.toString());
            }
        } else {
            msgOnline.setState(false);
            msgOnline.setMsg("授权字段不能为空");
        }

        return msgOnline;
    }

    public static class MsgOnline {
        boolean state;
        String msg;

        public boolean isState() {
            return state;
        }

        public void setState(boolean state) {
            this.state = state;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        @Override
        public String toString() {
            return "MsgOnline{" +
                    "state=" + state +
                    ", msg='" + msg + '\'' +
                    '}';
        }
    }

}
