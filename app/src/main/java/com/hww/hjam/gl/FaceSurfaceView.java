package com.hww.hjam.gl;

import android.content.Context;
import android.util.AttributeSet;

import com.imi.sdk.camera.ImageData;
import com.imi.sdk.utils.Rect;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class FaceSurfaceView extends BaseGLSurfaceView {
    private ImageData rgbImage;
    private Rect faceRect;

    private RgbRender rgbRender;
    private RectRender rectRender;

    public FaceSurfaceView(Context context) {
        super(context);
    }

    public FaceSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void updateColorImage(ImageData rgbImage){
        this.rgbImage = rgbImage;
    }

    public void updateFaceRect(Rect rect){
        this.faceRect = rect;
    }

    @Override
    public void onPause() {
        super.onPause();

        rgbImage = null;
        faceRect = null;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        super.onSurfaceCreated(gl, config);

        rgbRender = new RgbRender(getContext());
        rgbRender.createOnGLThread();

//        rectRender = new RectRender(getContext());
//        rectRender.createOnGLThread();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        super.onSurfaceChanged(gl, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        super.onDrawFrame(gl);

        if (rgbImage == null){
            return;
        }
        rgbRender.draw(rgbImage.getImageData());

//        if (faceRect != null){
//            rectRender.draw(faceRect, rgbImage.getImageWidth(), rgbImage.getImageHeight());
//        }
    }
}
