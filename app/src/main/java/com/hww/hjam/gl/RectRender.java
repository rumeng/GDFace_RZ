package com.hww.hjam.gl;

import android.content.Context;
import android.opengl.GLES20;

import com.hww.hjam.utils.ShaderUtils;
import com.imi.sdk.utils.Rect;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class RectRender {
    private static final String VERTEX_SHADER_NAME = "RectShader.vert";
    private static final String FRAGMENT_SHADER_NAME = "RectShader.frag";

    private static final int VERTEX_NUM = 4;
    private static final int COMPONENTS_PER_VERTEX = 3;
    private static final int BYTES_PER_FLOAT = 4;

    private Context context;
    private int programHandle;
    private int vertexHandle;
    private int colorHandle;

    private FloatBuffer vertexBuffer;

    private final float[] rectColor = {1.0f, 0.0f, 0.0f, 1.0f};

    public RectRender(Context context){
        this.context = context;

        vertexBuffer = ByteBuffer.allocateDirect(VERTEX_NUM * COMPONENTS_PER_VERTEX * BYTES_PER_FLOAT).order(ByteOrder.nativeOrder()).asFloatBuffer();
    }

    public void createOnGLThread(){
        int vertexShader = ShaderUtils.loadShader(context, VERTEX_SHADER_NAME, GLES20.GL_VERTEX_SHADER);
        int fragmentShader = ShaderUtils.loadShader(context, FRAGMENT_SHADER_NAME, GLES20.GL_FRAGMENT_SHADER);
        programHandle = ShaderUtils.createProgram(context, vertexShader, fragmentShader, false);

        vertexHandle = GLES20.glGetAttribLocation(programHandle, "rectPosition");
        colorHandle = GLES20.glGetAttribLocation(programHandle, "rectColor");
    }

    public void draw(Rect rect, int imageWidth, int imageHeight){
        // TODO 动态计算矩形框，目前计算有误
//        int halfWidth = imageWidth / 2;
//        int halfHeight = imageHeight / 2;
//
//        float X = rect.getWidth() / halfWidth;
//        float Y = rect.getHeight() / halfHeight;
//
//        // 左上角点
//        int topLeftX = (int) ((rect.getX() - halfWidth)/(float)halfWidth);
//        int topLeftY = -(int) ((rect.getY() - halfHeight)/(float)halfHeight);
//
//        vertexBuffer.clear();
//        vertexBuffer.position(0);
//        // 左上角点
//        vertexBuffer.put(topLeftX);
//        vertexBuffer.put(topLeftY);
//        vertexBuffer.put(0);
//        // 右上角点
//        vertexBuffer.put(topLeftX + X);
//        vertexBuffer.put(topLeftY);
//        vertexBuffer.put(0);
//        // 右下角点
//        vertexBuffer.put(topLeftX + X);
//        vertexBuffer.put(topLeftY - Y);
//        vertexBuffer.put(0);
//        // 左下角点
//        vertexBuffer.put(topLeftX);
//        vertexBuffer.put(topLeftY - Y);
//        vertexBuffer.put(0);
//        vertexBuffer.position(0);


        vertexBuffer.put(VERTEX);
        vertexBuffer.position(0);

        GLES20.glUseProgram(programHandle);

        GLES20.glVertexAttribPointer(vertexHandle, COMPONENTS_PER_VERTEX, GLES20.GL_FLOAT, false, 0, vertexBuffer);
        GLES20.glEnableVertexAttribArray(vertexHandle);

        GLES20.glVertexAttrib4fv(colorHandle, rectColor, 0);
        GLES20.glLineWidth(5);

        GLES20.glDrawArrays(GLES20.GL_LINE_LOOP, 0, 4);
    }

    // 屏幕中心固定位置矩形框
    private static float[] VERTEX = {
        -0.5f, 0.5f, 0.0f,
        0.5f, 0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f
    };
}
