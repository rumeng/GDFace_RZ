package com.hww.hjam.utils;

import android.content.Context;
import android.opengl.GLES20;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ShaderUtils {

    /**
     * Create GLES program from the specified shader file
     * warning: the shader file must be included in the assets file
     * warning: the {detachShader} parameter is not tested, you had better always use true.
     * @param context
     * @param vertexShaderFile
     * @param fragmentShaderFile
     * @param detachShader true: detach the shader after the program created.  false: do not detach
     * @return
     */
    public static int createProgram(Context context, String vertexShaderFile, String fragmentShaderFile, boolean detachShader){
        int vertexShaderHandle =  loadShader(context, vertexShaderFile, GLES20.GL_VERTEX_SHADER);
        int fragmentShaderHandle = loadShader(context, fragmentShaderFile, GLES20.GL_FRAGMENT_SHADER);
        return createProgram(context, vertexShaderFile, fragmentShaderFile, detachShader);
    }

    /**
     *
     * @param context
     * @param vertexShaderHandle
     * @param fragmentShaderHandle
     * @param detachShader
     * @return
     */
    public static int createProgram(Context context, int vertexShaderHandle, int fragmentShaderHandle, boolean detachShader){
        int programHandle = GLES20.glCreateProgram();
        GLES20.glAttachShader(programHandle, vertexShaderHandle);
        GLES20.glAttachShader(programHandle, fragmentShaderHandle);
        GLES20.glLinkProgram(programHandle);

        int[] linkResult = new int[1];
        GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkResult, 0);
        if (linkResult[0] == GLES20.GL_FALSE){
            Log.d("FaceDebug","Failed to create GLES20 program");
        }

        GLES20.glUseProgram(programHandle);

        if (detachShader){
            GLES20.glDetachShader(programHandle, vertexShaderHandle);
            GLES20.glDetachShader(programHandle, fragmentShaderHandle);
        }
        return programHandle;
    }

    /**
     * load and create shader from file
     * @param context
     * @param fileName which contains shader code
     * @param shaderType
     * @return shader handle
     */
    public static int loadShader(Context context, String fileName, int shaderType) {
        String shaderCode = null;
        try {
            shaderCode = readRawTextFromAssetsFile(context, fileName);
            if (TextUtils.isEmpty(shaderCode)) {
                Log.d("FaceDebug","The shader code read from" + fileName + " is empty, please check that");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int shaderHandle = GLES20.glCreateShader(shaderType);
        GLES20.glShaderSource(shaderHandle, shaderCode);
        GLES20.glCompileShader(shaderHandle);

        int[] compileResult = new int[1];
        GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, compileResult, 0);
        if (compileResult[0] == GLES20.GL_FALSE){
            Log.d("FaceDebug","Failed to create shader from source file " + fileName + "---" + GLES20.glGetShaderInfoLog(shaderHandle));
        }

        return shaderHandle;
    }

    /**
     * Read shader code from file
     *
     * @param context
     * @param fileName the asset file relative path that contains the shader code
     * @return the shader code
     */
    private static String readRawTextFromAssetsFile(Context context, String fileName) throws IOException{
        StringBuilder sb = new StringBuilder();
        String line;

        InputStream inputStream = context.getAssets().open(fileName);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line).append("\n");
        }

        return sb.toString();
    }
}
