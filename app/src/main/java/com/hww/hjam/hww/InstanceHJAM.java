package com.hww.hjam.hww;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.hww.gdface_rz.MyApplication;
import com.hww.gdface_rz.bean.MFrame;
import com.hww.gdface_rz.db.entity.User;
import com.hww.gdface_rz.db.utils.DBUserUtil;
import com.hww.gdface_rz.faceutil.Face_Mtf;
import com.hww.gdface_rz.saveUser;
import com.hww.gdface_rz.util.DataSwitch;
import com.hww.gdface_rz.util.ImageUtils;
import com.hww.gdface_rz.util.InfoShowIdCard;
import com.hww.gdface_rz.util.JudgeTool;
import com.hww.gdface_rz.util.SharePreferencesUtils;
import com.hww.gdface_rz.view.FaceMarkSurface;
import com.hww.hjam.gl.FaceSurfaceView;
import com.imi.sdk.camera.CameraConfig;
import com.imi.sdk.camera.CameraFrame;
import com.imi.sdk.camera.ImageData;
import com.imi.sdk.camera.ImiCamera;
import com.imi.sdk.camera.OnOpenCameraListener;
import com.imi.sdk.camera.Size;
import com.imi.sdk.face.OnSessionInitializeListener;
import com.imi.sdk.face.Session;
import com.imi.sdk.face.SessionConfig;
import com.imi.sdk.facebase.utils.NativeUtils;
import com.imi.sdk.facebase.utils.ResultCode;

import net.gdface.mtfsdk.NativeFaceInfo;
import net.gdface.utils.FaceUtilits;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author hww
 * @date 2019-12-17.
 * email：
 * description：
 */
public class InstanceHJAM {

    /**
     * 相机模组的封装类，封装了相机模组的打开及关闭等操作，该类是使用华捷艾米相机模组的唯一入口点
     */
    private ImiCamera imiCamera;
    /**
     * 该类对华捷艾米人脸算法进行了封装，提供了配置算法、初始化算法、运行算法的相关API。 该类是人脸算法的唯一入口点
     */
    private Session session;
    private Context context;
    /**
     * 渲染彩色图的SurfaceView
     */
    private FaceSurfaceView faceSurface;
    /**
     * 算法暂只支持640 * 480 分辨率
     */
    public static final int IMAGE_WIDTH = 640;
    public static final int IMAGE_HEIGHT = 480;

    private OpenState openState;

    private FaceMarkSurface markSurface;


    //是否开启活体
    private boolean isOpenLive = SharePreferencesUtils.getBoolean(MyApplication.getApplication(), "isOpenLive", true);

    public boolean isOpenLive() {
        return isOpenLive;
    }

    private boolean isOpenDetect = false;

    public boolean isOpenDetectMore() {
//        String key = "detectMore";
//        /*false为关闭状态*/
//        boolean state = SharePreferencesUtils.getBoolean(MyApplication.getApplication(), key);
//        state = !state;
//        SharePreferencesUtils.putBoolean(MyApplication.getApplication(), key, state);
        return isOpenDetect;
    }

    public void setOpenDetect(boolean openDetect) {
        isOpenDetect = openDetect;
    }

    public void setOpenLive(boolean openLive) {
        isOpenLive = openLive;
    }

    public InstanceHJAM(Context context, FaceSurfaceView faceSurface, OpenState openState, FaceMarkSurface markSurface) {
        this.context = context;
        this.faceSurface = faceSurface;
        this.openState = openState;
        this.markSurface = markSurface;
    }

    /**
     * 3D摄像头算法
     */
    private boolean state3D = false;

    public void openCamera() {
        imiCamera = new ImiCamera(context);

        imiCamera.open(new OnOpenCameraListener() {
            @Override
            public void onOpenCameraError(String s) {
                mLog("Open camera failed : " + s);
            }

            @Override
            public void onOpenCameraSuccess() {
                /*相机模组配置类*/
                CameraConfig cameraConfig = new CameraConfig();
                // 彩色图分辨率
                cameraConfig.colorSize = new Size(IMAGE_WIDTH, IMAGE_HEIGHT);
                // 深度图分辨率
                cameraConfig.depthSize = new Size(IMAGE_WIDTH, IMAGE_HEIGHT);
                // 彩色图和深度图配准，务必设置为true
                cameraConfig.imageRegistration = true;
                // 相机配置
                imiCamera.configure(cameraConfig);
                // 开启相机图像流
                imiCamera.startStream();

                ArrayList<Size> supportedColorSize = imiCamera.getSupportedColorSize();
                for (Size size : supportedColorSize) {
                    mLog("ColorSize : width =" + size.getWidth() + "  height = " + size.getHeight());
                }
                mLog("Open camera successfully");

                if (state3D) {
                    initFaceAlgorithm();
                } else {
                    openState.openSuccess();
                }

            }
        }, false);
    }

    // 初始化人脸算法，必须在成功打开相机后初始化
    private void initFaceAlgorithm() {
        // 代码中是调试路径，这里仅为了提示用户更换为自己的路径
//        File modelFile = new File("/sdcard/FaceConfig/");
//        if (!modelFile.exists()) {
//            Toast.makeText(context, "Please set the right model file path", Toast.LENGTH_LONG).show();
//        }


        //拷贝模型到SD卡中
        copyAssetsDir2Phone((Activity) context, "model");

        SessionConfig sessionConfig = new SessionConfig();
        // 设置相机设备句柄
        sessionConfig.cameraHandle = imiCamera.getCameraHandle();
        // 设置相机串号
        sessionConfig.cameraSn = imiCamera.getSn();
        // 设置算法模型文件路径
        String modelPath = ((Activity) context).getFilesDir().getAbsolutePath() + File.separator + "model" + File.separator;
        mLog("模型路径：" + modelPath);
        sessionConfig.modelPath = modelPath;

        session = new Session(context);
        // 设置算法配置
        session.configure(sessionConfig);
        session.initializeAsync(new OnSessionInitializeListener() {
            @Override
            public void onSessionInitialized(int i, String s) {
                if (i == ResultCode.OK) {
                    mLog("Initalize face algorithm successfully");
                    // 算法初始化成功
                    openState.openSuccess();
//                    startAlgThread();
                } else {
                    openState.openFail("Initalize face algorithm failed :" + s + ":Code is>>" + i);
                }
            }
        });
    }

    /**
     * 从assets目录中复制整个文件夹内容,考贝到 /data/data/包名/files/目录中
     *
     * @param activity activity 使用CopyFiles类的Activity
     * @param filePath String  文件路径,如：/assets/aa
     */
    private void copyAssetsDir2Phone(Activity activity, String filePath) {
        try {
            String[] fileList = activity.getAssets().list(filePath);
            if (fileList.length > 0) {//如果是目录
                File file = new File(activity.getFilesDir().getAbsolutePath() + File.separator + filePath);
                file.mkdirs();//如果文件夹不存在，则递归
                for (String fileName : fileList) {
                    copyAssetsDir2Phone(activity, filePath + File.separator + fileName);
                }
                mLog("模型文件复制完成");
            } else {//如果是文件
                InputStream inputStream = activity.getAssets().open(filePath);
                File file = new File(activity.getFilesDir().getAbsolutePath() + File.separator + filePath);
                mLog("copyAssets2Phone file:" + file.getAbsolutePath());
                if (!file.exists() || file.length() == 0) {
                    FileOutputStream fos = new FileOutputStream(file);
                    int len = -1;
                    byte[] buffer = new byte[1024];
                    while ((len = inputStream.read(buffer)) != -1) {
                        fos.write(buffer, 0, len);
                    }
                    fos.flush();
                    inputStream.close();
                    fos.close();
                    mLog("模型文件复制成功");
                } else {
                    mLog("模型文件已存在，无需复制");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // 开启相机线程
    private CameraThread cameraThread;

    public void startCameraThread() {
        mLog("startCameraThread()");
        if (cameraThread == null || !cameraThread.isRuning()) {
            mLog("CameraThread start()");
            cameraThread = new CameraThread();
            cameraThread.start();
        }
    }

    private class CameraThread extends Thread {
        private boolean isRun = true;
        private boolean skipFrame = true;//跳帧
        private ByteBuffer dataBGR = null;

        public boolean isRuning() {
            return isRun;
        }

        public void stopThread() {
            isRun = false;
            interrupt();
        }

        @Override
        public void run() {
            mLog("CameraThread run()");
            while (isRun && imiCamera != null) {

                /**
                 * 注意：{@link ImiCamera#updateFrame()} 返回的CameraFrame中彩色图和深度图持有的ByteBuffer是全局公用的，
                 * 也就是说无论调用多少次，彩色图和深度图都指向相同的ByteBuffer
                 * Demo仅演示SDK API的使用，未对多线程进行处理。
                 * 为了防止运行算法线程的图像数据被更改，建议用户对图像数据进行拷贝后再运行算法，拷贝图像可使用{@link ImageData#clone()}
                 * 基于性能考虑，建议用户不要每一帧都创建{@link ImageData}以拷贝数据, 最好使用对象池回收重用。
                 */
                CameraFrame cameraFrame = imiCamera.updateFrame();
                if (cameraFrame == null /*|| session == null*/) {
//                    mLog("cameraFrame == null");
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    continue;
                }
//                mLog("cameraFrame ok");

                faceSurface.updateColorImage(cameraFrame.getColorImage());
                faceSurface.requestRender();

                //跳帧
                if (skipFrame) {
                    skipFrame = false;
                    continue;
                }
                skipFrame = true;

                if (!Face_Mtf.getInstance().faceLicenseValid()) {
                    return;
                }

                ImageData colorImage = cameraFrame.getColorImage();
                if (dataBGR == null) {
                    dataBGR = ByteBuffer.allocateDirect(colorImage.getImageWidth() * colorImage.getImageHeight() * 3).order(ByteOrder.nativeOrder());
                }

                ImageData bgrImage = new ImageData(dataBGR, colorImage.getImageWidth(), colorImage.getImageHeight());
                NativeUtils.convertRgbToBgr(colorImage.getImageData(), dataBGR, colorImage.getImageWidth(), colorImage.getImageHeight());
                MFrame frame = new MFrame(cameraFrame.getColorImage(), cameraFrame.getDepthImage(), bgrImage);

                byte[] dataBGRArray = FaceUtilits.getBytesInBuffer(dataBGR);

                //人脸检测
//                long time3 = System.currentTimeMillis();
                NativeFaceInfo faceInfo = Face_Mtf.getInstance().faceDetect(dataBGRArray, colorImage.getImageWidth(), colorImage.getImageHeight(), 1);

                double state = 0;
                if (isOpenDetect) {
                    double[] faceRectInfo = new double[21];
//                    int faceSum = FaceApiMtfAndroid.getInstance().facePosInterrelate(null == faceInfo ? 0 : 1, 1, null == faceInfo ? new double[21] : faceInfo.getNativeData(), faceRectInfo);
                    state = faceRectInfo[20];
                }


                if (faceInfo != null) {
//                    mLog("人脸检测用时："+(System.currentTimeMillis()-time3));
                    android.graphics.Rect rect = new android.graphics.Rect(faceInfo.getPos().getLeft(),
                            faceInfo.getPos().getTop(),
                            faceInfo.getPos().getLeft() + faceInfo.getPos().getWidth(),
                            faceInfo.getPos().getTop() + faceInfo.getPos().getHeight());
                    markSurface.drawInfo(rect, colorImage.getImageWidth(), colorImage.getImageHeight());
                } else {
//                    mLog("没有脸");
                    markSurface.clearDrawInfo();
                }

                if (faceInfo != null) {

                    InfoShowIdCard idCardInfo = rzIDCardInfo;

                    if (isOpenLive) {
                        if (!isOpenDetect) {
                            if (codeInfoIdCardD == null) {
                                continue;
                            }
                        }
                        long time1 = System.currentTimeMillis();
                        com.imi.sdk.utils.Rect rect1 = new com.imi.sdk.utils.Rect(faceInfo.getPos().getLeft(), faceInfo.getPos().getTop(), faceInfo.getPos().getWidth(), faceInfo.getPos().getHeight());
                        float live = frame.detectLiveness(rect1);
                        mLog("live:" + live + " 用时：" + (System.currentTimeMillis() - time1));
                        if (Double.doubleToLongBits(live) <= Double.doubleToLongBits(0.6d)) {
                            mLog("live:" + live + " 用时：" + (System.currentTimeMillis() - time1));
                            if (Double.doubleToLongBits(live) <= Double.doubleToLongBits(0.4d)) {
                                codeInfoIdCard = null;
                                codeInfoIdCardD = null;
                                byte[] colorBytesInBuffer = FaceUtilits.getBytesInBuffer(colorImage.getImageData());
                                Bitmap bitmap = com.hww.gdface_rz.util.ImageData.rgb2Bitmap(colorBytesInBuffer, colorImage.getImageWidth(), colorImage.getImageHeight());
                                if (rzbdCallback != null) {
                                    rzbdCallback.failed(idCardInfo, bitmap, -100);
                                }
                                continue;
                            }
                        }
                    }

                    if (isOpenDetect) {
                        if (state != 1) {
                            //提取人脸特征
                            long timeFea = System.currentTimeMillis();
                            double[] feaB = Face_Mtf.getInstance().faceFeature(dataBGRArray, colorImage.getImageWidth(), colorImage.getImageHeight(), faceInfo);
                            mLog("提取人脸特征用时B：" + (System.currentTimeMillis() - timeFea));
                            detectMore(feaB);
                        }
                    } else {
                        if (codeInfoIdCardD != null) {
                            //提取人脸特征
                            long timeFea = System.currentTimeMillis();
                            double[] feaB = Face_Mtf.getInstance().faceFeature(dataBGRArray, colorImage.getImageWidth(), colorImage.getImageHeight(), faceInfo);
                            mLog("提取人脸特征用时：" + (System.currentTimeMillis() - timeFea));

                            double source = Face_Mtf.getInstance().compareCode(feaB, codeInfoIdCardD);
                            mLog("人证分数:" + source);

                            if (Double.doubleToLongBits(source) > Double.doubleToLongBits(0.42d)) {
                                codeInfoIdCard = null;
                                codeInfoIdCardD = null;
                                byte[] colorBytesInBuffer = FaceUtilits.getBytesInBuffer(colorImage.getImageData());
                                Bitmap bitmap = com.hww.gdface_rz.util.ImageData.rgb2Bitmap(colorBytesInBuffer, colorImage.getImageWidth(), colorImage.getImageHeight());
                                if (rzbdCallback != null) {
                                    idCardInfo.setBitmap(bitmap);
                                    idCardInfo.setFeature(feaB);
                                    MyApplication.DEFAULT_SINGLE_EXECUTOR_DB.execute(new saveUser(idCardInfo));
                                    rzbdCallback.success(idCardInfo, bitmap, source);
                                }
                            } else {
                                //累计3次无法人证识别，判定为无法识别
                                rzFailedTime++;
                                if (rzFailedTime >= 3) {
                                    codeInfoIdCard = null;
                                    codeInfoIdCardD = null;
                                    byte[] colorBytesInBuffer = FaceUtilits.getBytesInBuffer(colorImage.getImageData());
                                    Bitmap bitmap = com.hww.gdface_rz.util.ImageData.rgb2Bitmap(colorBytesInBuffer, colorImage.getImageWidth(), colorImage.getImageHeight());
                                    if (rzbdCallback != null) {
                                        rzbdCallback.failed(idCardInfo, bitmap, source);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            mLog("CameraThread end");
            isRun = false;
        }
    }

    private double valueDetect = 0.6d;
    private int totalDetect = 0;

    private void detectMore(double[] feaB) {
        DBUserUtil.getInstance().readLockPerson.lock();
        try {
            for (Iterator<User> userIterator = DBUserUtil.getInstance().getPersonList().iterator(); userIterator.hasNext(); ) {
                User next = userIterator.next();
                byte[] feature = next.getFeature();
                if (null == feature) {
                    continue;
                }
                double[] doubles = DataSwitch.byte2double(feature);
                double source = Face_Mtf.getInstance().compareCode(feaB, doubles);
                if (Double.doubleToLongBits(source) > Double.doubleToLongBits(valueDetect)) {
                    String idCard = next.getIdCard();
                    boolean b = JudgeTool.getInstance().judgeDetectIntervalTime(idCard, System.currentTimeMillis());
                    if (b) {
                        if (rzbdCallback != null) {
                            Bitmap bitmap = ImageUtils.getBitmap(next.getPhoto(), 0);
                            InfoShowIdCard infoShowIdCard = new InfoShowIdCard(next.getName(), idCard, next.getValidityTime(), bitmap);
                            rzbdCallback.successMore(infoShowIdCard);
                            totalDetect = 0;
                            try {
//                                FaceApiMtfAndroid.getInstance().setFacePosStatus(0, 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return;
                        }
                    }
                }
            }
            totalDetect++;
            if (totalDetect == 10) {
                rzbdCallback.failedMore();
            }
        } finally {
            DBUserUtil.getInstance().readLockPerson.unlock();
        }
    }

    /**
     * @方法描述 将RGB字节数组转换成Bitmap，
     */
    static public Bitmap bgr2Bitmap(byte[] data, int width, int height) {
        int[] colors = convertByteToColor(data);    //取RGB值转换为int数组
        if (colors == null) {
            return null;
        }

        Bitmap bmp = Bitmap.createBitmap(colors, 0, width, width, height,
                Bitmap.Config.ARGB_8888);
        return bmp;
    }

    // 将一个byte数转成int
// 实现这个函数的目的是为了将byte数当成无符号的变量去转化成int
    public static int convertByteToInt(byte data) {

        int heightBit = (int) ((data >> 4) & 0x0F);
        int lowBit = (int) (0x0F & data);
        return heightBit * 16 + lowBit;
    }


    // 将纯RGB数据数组转化成int像素数组
    public static int[] convertByteToColor(byte[] data) {
        int size = data.length;
        if (size == 0) {
            return null;
        }

        int arg = 0;
        if (size % 3 != 0) {
            arg = 1;
        }

        // 一般RGB字节数组的长度应该是3的倍数，
        // 不排除有特殊情况，多余的RGB数据用黑色0XFF000000填充
        int[] color = new int[size / 3 + arg];
        int red, green, blue;
        int colorLen = color.length;
        if (arg == 0) {
            for (int i = 0; i < colorLen; ++i) {
                red = convertByteToInt(data[i * 3 + 2]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 0]);

                // 获取RGB分量值通过按位或生成int的像素值
                color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
            }
        } else {
            for (int i = 0; i < colorLen - 1; ++i) {
                red = convertByteToInt(data[i * 3]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 2]);
                color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
            }

            color[colorLen - 1] = 0xFF000000;
        }

        return color;
    }

    private InfoShowIdCard rzIDCardInfo;
    private byte[] codeInfoIdCard;
    private double[] codeInfoIdCardD;
    private RzbdCallback rzbdCallback;
    private int rzFailedTime = 0;

    public void rzbd(InfoShowIdCard rzIDCardInfo, byte[] codeInfoIdCard, RzbdCallback rzbdCallback) {
        rzFailedTime = 0;
        this.rzIDCardInfo = rzIDCardInfo;
        this.codeInfoIdCard = codeInfoIdCard;
        this.rzbdCallback = rzbdCallback;
    }

    public void rzbd(InfoShowIdCard rzIDCardInfo, double[] codeInfoIdCard, RzbdCallback rzbdCallback) {
        rzFailedTime = 0;
        this.rzIDCardInfo = rzIDCardInfo;
        this.codeInfoIdCardD = codeInfoIdCard;
        this.rzbdCallback = rzbdCallback;
    }

    public void setRzbdCallback(RzbdCallback rzbdCallback) {
        this.rzbdCallback = rzbdCallback;
    }

    public void close() {
        if (cameraThread != null) {
            cameraThread.stopThread();
            cameraThread = null;
        }

        if (imiCamera != null) {
            imiCamera.closeCamera();
        }

        if (session != null) {
            session.release();
        }
    }

    public interface OpenState {
        /**
         * 打开摄像头成功
         */
        public void openSuccess();

        /**
         * 打开摄像头识别
         */
        public void openFail(String fail);
    }

    public interface RzbdCallback {
        public void success(InfoShowIdCard idCardInfo, Bitmap img, double score);

        public void successMore(InfoShowIdCard idCardInfo);

        public void failedMore();

        public void failed(InfoShowIdCard idCardInfo, Bitmap img, double score);

    }

    private void mLog(String con) {
//        Log.i("hww", "FA-->" + con);
    }
}
