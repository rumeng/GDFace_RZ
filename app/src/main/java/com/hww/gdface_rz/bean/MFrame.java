package com.hww.gdface_rz.bean;

import com.imi.sdk.camera.ImageData;
import com.imi.sdk.face.Frame;

public class MFrame extends Frame {
    public MFrame(ImageData rgbImage, ImageData depthImage, ImageData bgrImage) {
        super(rgbImage,depthImage,bgrImage);
    }
}
