package com.hww.gdface_rz;

import android.app.Application;
import android.os.Looper;
import android.os.Message;

import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.hww.gdface_rz.db.utils.DaoManager;
import com.hww.gdface_rz.event.EventType;
import com.hww.gdface_rz.event.GlobalHandler;
import com.hww.gdface_rz.util.FastYUVtoRGB;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author hww
 * @date 2019-12-13.
 * email：
 * description：
 */
public class MyApplication extends Application {
    private static MyApplication myApplication;

    public static MyApplication getApplication() {
        return myApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = this;
        FastYUVtoRGB.getInstance().initContext(this);

        //初始化线程池
        initThreadManager();

        //全局Handle
        globalHandler = new GlobalHandler(Looper.getMainLooper());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DaoManager.getInstance().closeConnection();
    }

    public static final ExecutorService DEFAULT_SINGLE_EXECUTOR_DB = MoreExecutors.getExitingExecutorService(
            new ThreadPoolExecutor(1, 1,
                    0L, TimeUnit.MILLISECONDS,
                    new LinkedBlockingQueue<Runnable>(),
                    new ThreadFactoryBuilder().setNameFormat("database-%d").build()));

    static {
        // 程序退出时自动销毁某些资源
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                MyApplication.getApplication().distoryGlobalHandler();

                MyApplication.getApplication().distoryThreadManager();
            }
        });
    }

    //========================================================================
    private ThreadPoolExecutor threadManger;//线程池

    //在线程池的线程中执行Runnable
    public void executeInThreadPool(Runnable runnable) {
        if (threadManger != null) {
            threadManger.submit(runnable);
        }
    }

    //初始化线程池
    private void initThreadManager() {
        //线程缓存60秒
        threadManger = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                60L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>(),
                new ThreadFactoryBuilder().setNameFormat("gd-%d").build());
    }

    //销毁线程池
    public void distoryThreadManager() {
        if (threadManger != null) {
            threadManger.shutdown();
            threadManger = null;
        }
    }

    //============================== 全局Handler ========================================
    private GlobalHandler globalHandler;

    public GlobalHandler getGlobalHandler() {
        return globalHandler;
    }

    /**
     * 销毁GlobalHandler
     */
    public void distoryGlobalHandler() {
        if (globalHandler != null) {
            globalHandler.removeCallbacksAndMessages(null);
            globalHandler = null;
        }
    }

    public void addGlobalHandlerListener(GlobalHandler.HandleMsgListener listener) {
        if (globalHandler == null) return;
        globalHandler.addListener(listener);
    }

    public void removeGlobalHandlerListener(GlobalHandler.HandleMsgListener listener) {
        if (globalHandler == null) return;
        globalHandler.removeListener(listener);
    }

    /**
     * 移除消息
     */
    public void removeMsg(int what) {
        if (globalHandler == null) return;
        globalHandler.removeMessages(what);
    }

    /**
     * 发送消息（到主线程）
     */
    public void sendMsg(int what) {
        if (globalHandler == null) return;
        globalHandler.obtainMessage(what).sendToTarget();
    }

    /**
     * 发送消息（到主线程）
     */
    public void sendMsg(int what, Object data) {
        if (globalHandler == null) return;
        globalHandler.obtainMessage(what, data).sendToTarget();
    }

    /**
     * 发送消息（到主线程）
     */
    public void sendMsg(int what, int arg1) {
        if (globalHandler == null) return;
        globalHandler.obtainMessage(what, arg1, 0).sendToTarget();
    }

    /**
     * 发送消息（到主线程）
     */
    public void sendMsg(int what, int arg1, Object data) {
        if (globalHandler == null) return;
        globalHandler.obtainMessage(what, arg1, 0, data).sendToTarget();
    }

    /**
     * 延迟发送消息（到主线程）
     */
    public void sendMsgDelayed(int what, long delayMillis) {
        if (globalHandler == null) return;
        globalHandler.sendEmptyMessageDelayed(what, delayMillis);
    }

    /**
     * 延迟发送消息（到主线程）
     */
    public void sendMsgDelayed(int what, Object data, long delayMillis) {
        if (globalHandler == null) return;
        Message msg = globalHandler.obtainMessage();
        msg.what = what;
        msg.obj = data;
        globalHandler.sendMessageDelayed(msg, delayMillis);
    }

    /**
     * 延迟在子线程执行Runnable
     */
    public void executeRunnableDelayed(int what, Runnable runnable, long delayMillis) {
        if (globalHandler == null) return;
        if (delayMillis == 0) {
            executeInThreadPool(runnable);
            return;
        }

        Message msg = globalHandler.obtainMessage();
        msg.what = what;
        msg.obj = runnable;
        msg.arg1 = 0;
        globalHandler.sendMessageDelayed(msg, delayMillis);
    }

    /**
     * 延迟在子线程执行Runnable
     */
    public void executeRunnableDelayed(Runnable runnable, long delayMillis) {
        if (globalHandler == null) return;
        if (delayMillis == 0) {
            executeInThreadPool(runnable);
            return;
        }

        Message msg = globalHandler.obtainMessage();
        msg.what = EventType.EXECUTE_RUNNABLE;
        msg.obj = runnable;
        msg.arg1 = 0;
        globalHandler.sendMessageDelayed(msg, delayMillis);
    }

    /**
     * 延迟在主线程执行Runnable
     */
    public void executeRunnableOnMainDelayed(int what, Runnable runnable, long delayMillis) {
        if (globalHandler == null) return;
        if (delayMillis == 0) {
            globalHandler.obtainMessage(what, runnable).sendToTarget();
            return;
        }

        Message msg = globalHandler.obtainMessage();
        msg.what = what;
        msg.obj = runnable;
        msg.arg1 = 1;
        globalHandler.sendMessageDelayed(msg, delayMillis);
    }

    /**
     * 延迟在主线程执行Runnable
     */
    public void executeRunnableOnMainDelayed(Runnable runnable, long delayMillis) {
        if (globalHandler == null) return;
        if (delayMillis == 0) {
            globalHandler.obtainMessage(EventType.EXECUTE_RUNNABLE, runnable).sendToTarget();
            return;
        }

        Message msg = globalHandler.obtainMessage();
        msg.what = EventType.EXECUTE_RUNNABLE;
        msg.obj = runnable;
        msg.arg1 = 1;
        globalHandler.sendMessageDelayed(msg, delayMillis);
    }
}
