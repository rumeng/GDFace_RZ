//package com.hww.gdface_rz;
//
//import android.annotation.SuppressLint;
//import android.graphics.Bitmap;
//import android.graphics.Color;
//import android.hardware.Camera;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.util.Log;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.SurfaceView;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.hww.gdface_rz.util.BGRDataInfo;
//import com.hww.gdface_rz.util.FastYUVtoRGB;
//import com.hww.gdface_rz.util.SharePreferencesUtils;
//import com.hww.gdface_rz.util.ToolCameraData;
//import com.hww.gdface_rz.view.CustomPopWindow;
//import com.hww.gdface_rz.view.FaceMarkSurface;
//import com.hww.mtf.MTFRegister;
//import com.zkteco.android.IDReader.ConnectDeviceParam;
//import com.zkteco.android.IDReader.ZKInstance;
//import com.zkteco.android.biometric.core.device.TransportType;
//import com.zkteco.android.biometric.module.idcard.meta.IDCardInfo;
//
//import net.gdface.mtfsdk.FaceApiMtfAndroid;
//import net.gdface.mtfsdk.NativeFaceInfo;
//import net.gdface.sdk.CodeInfo;
//import net.gdface.sdk.NotFaceDetectedException;
//
//import java.util.Arrays;
//import java.util.concurrent.Callable;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.Semaphore;
//
//
///**
// * @author hww
// * @date 2019-12-13.
// * email：
// * description：
// */
//public class DoubleFacingFragment extends Fragment {
//
//    private ZKInstance zkInstance;
//    private CameraUtil cameraUtil;
//
//    ExecutorService executorService;
//
//    private CustomPopWindow popCustom;
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        ConnectDeviceParam connectDeviceParam = new ConnectDeviceParam();
//        connectDeviceParam.setTransportType(TransportType.USB);
//        connectDeviceParam.setUsbVID(1024);
//        connectDeviceParam.setUsbPID(50010);
//        zkInstance = new ZKInstance(getActivity(), connectDeviceParam, readCardCallback);
//
//        executorService = Executors.newSingleThreadExecutor();
//    }
//
//
//    private FaceMarkSurface faceMarkSurface;
//    private View mView;
//    private TextView tvName, tvNum, tvTime, tvHint, tvSDK;
//    private ImageView ivIdCard, ivCamera;
//    private final static int LABEL_SHOW_CARD_INFO = 0x80;
//    private final static int LABEL_SHOW_CARD_HEAD = 0x81;
//    private final static int LABEL_SHOW_CAMERA_HEAD = 0x82;
//    private final static int LABEL_CAMERA_OPEN = 0x83;
//    private final static int LABEL_SHOW_HINT = 0x84;
//    private final static int LABEL_SHOW_CLEAR_INFO = 0x85;
//    /**
//     * 保存上一次读取的多分钟号码，用于判断是否为重复读卡
//     * 人证比对识别的情况清空，视为重新刷卡
//     */
//    private String idCardNum = "";
//    /**
//     * 身份证照片特征码
//     */
//    private byte[] codeInfoIdCard;
//
//    /**
//     * 人证比对次数(每刷一次身份证，有三次比对机会)
//     */
//    private static int identifySum = 0;
//
//    private Semaphore semaphore = new Semaphore(1);
//
//    private TextView popNagative, popPositive;
//    private EditText popEdittext;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_main_double_facing, container, false);
//        mView = view.findViewById(R.id.fragment_double);
//        tvName = view.findViewById(R.id.tv_name);
//        tvNum = view.findViewById(R.id.tv_id_card_num);
//        tvTime = view.findViewById(R.id.tv_validityTime);
//        tvHint = view.findViewById(R.id.tv_hint);
//        tvSDK = view.findViewById(R.id.tv_init_SDK);
//        ivIdCard = view.findViewById(R.id.iv_id_card);
//        ivCamera = view.findViewById(R.id.iv_face);
//        SurfaceView surfaceView = view.findViewById(R.id.surfaceView);
//        faceMarkSurface = view.findViewById(R.id.faceMarkSurface);
//        cameraUtil = new CameraUtil(0, 0, new CameraUtil.Size(640, 480), surfaceView, previewCallback);
//        handler.sendEmptyMessageDelayed(LABEL_CAMERA_OPEN, 1500);
//        tvSDK.setOnClickListener(clickListener);
//
//        View popView = LayoutInflater.from(getActivity()).inflate(R.layout.pop_license, null);
//        popCustom = new CustomPopWindow.PopupWindowBuilder(getActivity())
//                .setView(popView)
//                .setBgDarkAlpha(0.8f)
//                .setFocusable(true)
//                .setOutsideTouchable(false).create();
//        popEdittext = popView.findViewById(R.id.pop_txt_dialog_tip);
//        popNagative = popView.findViewById(R.id.pop_btn_selectNegative);
//        popPositive = popView.findViewById(R.id.pop_btn_selectPositive);
//        popNagative.setOnClickListener(clickListener);
//        popPositive.setOnClickListener(clickListener);
//        return view;
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        zkInstance.startReadIDCard();
//        initMTFSDK(null);
//    }
//
//    private void initMTFSDK(final String licenseKey) {
//        executorService.execute(new Runnable() {
//            @Override
//            public void run() {
//                String key;
//                if (null == licenseKey) {
//                    key = SharePreferencesUtils.getString(getActivity(), MTFRegister.LICENSE_KEY);
//                } else {
//                    key = licenseKey;
//                }
//                final MTFRegister.MsgOnline online = MTFRegister.online(getActivity(), key);
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        tvSDK.setVisibility(View.VISIBLE);
//                        if (online.isState()) {
//                            tvSDK.setVisibility(View.GONE);
//                            popDissmiss();
//                        } else {
//                            tvSDK.setTextColor(Color.RED);
//                            tvSDK.setText("算法授权失败");
//                        }
//                        mToast(online.getMsg());
//                    }
//                });
//            }
//        });
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        cameraUtil.closeCamera();
//        zkInstance.closeReadIDCard();
//    }
//
//    View.OnClickListener clickListener = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            switch (v.getId()) {
//                case R.id.tv_init_SDK:
//                    if (!FaceApiMtfAndroid.licenseManager().isValidLicense()) {
//                        popCustom.showAtLocation(mView, Gravity.CENTER, 0, 0);
//                    }
//                    break;
//                case R.id.pop_btn_selectNegative:
//                    mToast("取消");
//                    popDissmiss();
//                    break;
//                case R.id.pop_btn_selectPositive:
//                    //授权
//                    String license = popEdittext.getText().toString().trim();
//                    if (null != license && license.length() > 0) {
//                        initMTFSDK(license);
//                    } else {
//                        mToast("授权码不能为空");
//                    }
//                    break;
//                default:
//                    break;
//            }
//        }
//    };
//
//    private void popDissmiss() {
//        if (null != popCustom) {
//            popCustom.dissmiss();
//        }
//    }
//
//    private void mToast(String con) {
//        Toast.makeText(getActivity(), con, Toast.LENGTH_SHORT).show();
//    }
//
//
//    @SuppressLint("HandlerLeak")
//    Handler handler = new Handler() {
//        @Override
//        public void handleMessage(@NonNull Message msg) {
//            switch (msg.what) {
//                case LABEL_SHOW_CARD_INFO:
//                    if (IDCardInfo.class.isInstance(msg.obj)) {
//                        IDCardInfo idCardInfo = (IDCardInfo) msg.obj;
//                        String name = getString(R.string.show_name) + idCardInfo.getName();
//                        String id = getString(R.string.show_id_card_num) + idCardInfo.getId();
//                        String avlidityTime = getString(R.string.show_validityTime) + idCardInfo.getValidityTime();
//
//                        tvName.setText(name);
//                        tvNum.setText(id);
//                        tvTime.setText(avlidityTime);
//                    } else {
//                        mLog("**********************************");
//                        tvName.setText(getString(R.string.show_name));
//                        tvNum.setText(getString(R.string.show_id_card_num));
//                        tvTime.setText(getString(R.string.show_validityTime));
//                    }
//                    break;
//                case LABEL_SHOW_CARD_HEAD:
//                    if (Bitmap.class.isInstance(msg.obj)) {
//                        ivIdCard.setImageBitmap((Bitmap) msg.obj);
//                    } else {
//                        ivIdCard.setImageResource(R.mipmap.idcard);
//                    }
//                    break;
//                case LABEL_SHOW_CAMERA_HEAD:
//                    if (Bitmap.class.isInstance(msg.obj)) {
//                        ivCamera.setImageBitmap((Bitmap) msg.obj);
//                    } else {
//                        ivCamera.setImageResource(R.mipmap.face);
//                    }
//                    break;
//                case LABEL_CAMERA_OPEN:
//                    cameraUtil.openCamera();
//                    break;
//                case LABEL_SHOW_HINT:
//                    tvHint.setText(null == msg.obj ? getString(R.string.rzbd) : String.valueOf(msg.obj));
//                    break;
//                case LABEL_SHOW_CLEAR_INFO:
//                    Object obj = msg.obj;
//                    long time = 10000;
//                    if (null != obj) {
//                        if (obj.equals("1")) {
//                            time = 3500;
//                        } else if (obj.equals("2")) {
//                            time = 2500;
//                        }
//                    }
//                    clearShowIdCardInfo(time);
//                    break;
//                default:
//                    break;
//            }
//        }
//    };
//
//    /**
//     * 情况界面信息显示
//     * 清空之前倒计时修改显示的机会，重新倒计时
//     *
//     * @param clearTime
//     */
//    private void clearShowIdCardInfo(long clearTime) {
//        handler.sendEmptyMessageDelayed(LABEL_SHOW_CARD_INFO, clearTime);
//        handler.sendEmptyMessageDelayed(LABEL_SHOW_CARD_HEAD, clearTime);
//        handler.sendEmptyMessageDelayed(LABEL_SHOW_CAMERA_HEAD, clearTime);
//        handler.sendEmptyMessageDelayed(LABEL_SHOW_HINT, clearTime);
//    }
//
//    ZKInstance.ReadCardCallback readCardCallback = new ZKInstance.ReadCardCallback() {
//        @Override
//        public void readCardSuccess(final IDCardInfo idCardInfo, final Bitmap headImg) {
//            mLog("readCardSuccess  " + idCardInfo.getName());
//            /*判断前后刷卡的是否为同一个人*/
//            if (!idCardNum.equals(idCardInfo.getId())) {
//                handler.obtainMessage(LABEL_SHOW_CARD_INFO, idCardInfo).sendToTarget();
//                handler.obtainMessage(LABEL_SHOW_CARD_HEAD, headImg).sendToTarget();
//                handler.sendEmptyMessage(LABEL_SHOW_CLEAR_INFO);
//
////                executorService.execute(new Runnable() {
////                    @Override
////                    public void run() {
//
//                //提取身份证照的人脸特征
//                CodeInfo[] codeInfos = null;
//                try {
//                    codeInfos = FaceApiMtfAndroid.getInstance().detectAndGetCodeInfo(headImg, 1);
//                } catch (NotFaceDetectedException e) {
//                    e.printStackTrace();
//                }
//                if (codeInfos != null && codeInfos.length == 1) {
//                    mLog("readCardSuccess  身份证照片特征提取成功");
//                    idCardNum = idCardInfo.getId();
//                    codeInfoIdCard = codeInfos[0].getCode();
//                    //身份证照片特征提取成功，进入摄像头人脸采集过程
//                    identifySum = 3;
//                }
////                    }
////                });
//            }
//        }
//
//        @Override
//        public void readCardFailed(String msg) {
//        }
//
//        @Override
//        public void openSuccess() {
//            handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.state_xxcj)).sendToTarget();
//        }
//
//        @Override
//        public void openFailed(String message) {
//        }
//    };
//
//    Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
//        @Override
//        public void onPreviewFrame(byte[] data, Camera camera) {
//            if (!FaceApiMtfAndroid.licenseManager().isValidLicense()) {
//                return;
//            }
//            Camera.Size size = camera.getParameters().getPreviewSize();
//            final Bitmap bitmap = FastYUVtoRGB.getInstance().convertYUVtoRGB(data, size.width, size.height);
//            BGRDataInfo dataInfo = ToolCameraData.getCameraBGRDataInfo(bitmap);
//
//            final NativeFaceInfo faceInfo = FaceApiMtfAndroid.getInstance().nativeDetectMaxFace(dataInfo.getData(), dataInfo.getWidth(), dataInfo.getHeight(), 1);
//            if (null != faceInfo) {
//                boolean[] label = new boolean[]{false};
//                faceMarkSurface.drawInfo(Arrays.asList((CodeInfo) faceInfo), label, dataInfo.getWidth(), dataInfo.getHeight());
//
//                /*identifyRZHY(dataInfo, faceInfo, bitmap);*/
//
//                if (identifySum > 0 && identifySum <= 3 && null != codeInfoIdCard) {
//                    handler.obtainMessage(LABEL_SHOW_CAMERA_HEAD, bitmap).sendToTarget();
//                    handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.state_rljc)).sendToTarget();
//
//                    byte[] codeInfo = FaceApiMtfAndroid.getInstance().nativeGetFaceFeature(dataInfo.getData(), dataInfo.getWidth(), dataInfo.getHeight(), faceInfo);
//                    double source = FaceApiMtfAndroid.getInstance().compareCode(codeInfo, codeInfoIdCard);
//                    mLog("source = " + source + "   num = " + identifySum);
//                    if (source > 0 && Double.doubleToLongBits(source) > Double.doubleToLongBits(0.45)) {
//                        /*提示比对成功*/
//                        handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.rz_success)).sendToTarget();
//                        handler.obtainMessage(LABEL_SHOW_CLEAR_INFO, "1").sendToTarget();
//                        idCardNum = "";
//                        identifySum = 0;
//                    } else {
//                        identifySum--;
//                        if (0 == identifySum) {
//                            /*提示比对识别*/
//                            handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.rz_failed)).sendToTarget();
//                            idCardNum = "";
//                            handler.obtainMessage(LABEL_SHOW_CLEAR_INFO, "2").sendToTarget();
//                        }
//                    }
//                } else {
//                    identifySum = 0;
//                }
//            } else {
//                faceMarkSurface.clearDrawInfo();
//            }
//        }
//    };
//
//    private void identifyRZHY(BGRDataInfo dataInfo, NativeFaceInfo faceInfo, Bitmap bitmap) {
//        synchronized (MyRunnable.class) {
//            executorService.execute(new MyRunnable(dataInfo, faceInfo, bitmap));
//        }
//    }
//
//    class MyCallable implements Callable<Boolean> {
//        @Override
//        public Boolean call() throws Exception {
//            return false;
//        }
//    }
//
//    class MyRunnable implements Runnable {
//        BGRDataInfo dataInfo;
//        NativeFaceInfo faceInfo;
//        Bitmap bitmap;
//
//        public MyRunnable(BGRDataInfo dataInfo, NativeFaceInfo faceInfo, Bitmap bitmap) {
//            this.dataInfo = dataInfo;
//            this.faceInfo = faceInfo;
//            this.bitmap = bitmap;
//        }
//
//        @Override
//        public void run() {
//            if (identifySum > 0 && identifySum <= 3 && null != codeInfoIdCard) {
//                handler.obtainMessage(LABEL_SHOW_CAMERA_HEAD, bitmap).sendToTarget();
//                handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.state_rljc)).sendToTarget();
//
//                byte[] codeInfo = FaceApiMtfAndroid.getInstance().nativeGetFaceFeature(dataInfo.getData(), dataInfo.getWidth(), dataInfo.getHeight(), faceInfo);
//                double source = FaceApiMtfAndroid.getInstance().compareCode(codeInfo, codeInfoIdCard);
//                mLog("source = " + source + "   num = " + identifySum);
//                if (source > 0 && Double.doubleToLongBits(source) > Double.doubleToLongBits(0.45)) {
//                    /*提示比对成功*/
//                    handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.rz_success)).sendToTarget();
//                    handler.obtainMessage(LABEL_SHOW_CLEAR_INFO, "1").sendToTarget();
//                    idCardNum = "";
//                    identifySum = 0;
//                } else {
//                    identifySum--;
//                    if (0 == identifySum) {
//                        /*提示比对识别*/
//                        handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.rz_failed)).sendToTarget();
//                        idCardNum = "";
//                        handler.obtainMessage(LABEL_SHOW_CLEAR_INFO, "2").sendToTarget();
//                    }
//                }
//            } else {
//                identifySum = 0;
//            }
//        }
//    }
//
//    private void mLog(String con) {
//        Log.i("hww", "DoubleFacingFragment " + con);
//    }
//}
