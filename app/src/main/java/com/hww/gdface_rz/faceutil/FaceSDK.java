package com.hww.gdface_rz.faceutil;

import android.content.Context;

import com.hww.mtf.MTFRegister;

import net.gdface.mtfsdk.NativeFaceInfo;
import net.gdface.sdk.CodeInfo;

public interface FaceSDK {

    MTFRegister.MsgOnline faceRegister(Context context, String licenseKey);

    boolean faceLicenseValid();

    /**
     * @param imgMatrix
     * @param width
     * @param height
     * @param detectMod 0:用于检测图片,1:用于视频的循环检测
     */
    NativeFaceInfo faceDetect(byte[] imgMatrix, int width, int height, int detectMod);


    double[] faceFeature(byte[] imgMatrix, int width, int height, CodeInfo faceInfo);

    double compareCode(double[] code1, double[] code2);

}
