package com.hww.gdface_rz.faceutil;

import android.content.Context;

import com.hww.mtf.MTFRegister;

import net.gdface.mtfsdk.FaceApiMtfAndroid;
import net.gdface.mtfsdk.NativeFaceInfo;
import net.gdface.sdk.CodeInfo;

public class Face_Mtf implements FaceSDK {

    private static final Face_Mtf faceMtf = new Face_Mtf();

    public static Face_Mtf getInstance() {
        return faceMtf;
    }


    @Override
    public MTFRegister.MsgOnline faceRegister(Context context, String licenseKey) {
        return MTFRegister.online(context, licenseKey);
    }

    @Override
    public boolean faceLicenseValid() {
        return FaceApiMtfAndroid.licenseManager().isValidLicense();
    }

    @Override
    public NativeFaceInfo faceDetect(byte[] imgMatrix, int width, int height, int detectMod) {
        return FaceApiMtfAndroid.getInstance().nativeDetectMaxFace(imgMatrix, width, height, detectMod);
    }

    @Override
    public double[] faceFeature(byte[] imgMatrix, int width, int height, CodeInfo faceInfo) {
        return FaceApiMtfAndroid.getInstance().nativeGetFaceFeatureD(imgMatrix, width, height, faceInfo);
    }

    @Override
    public double compareCode(double[] code1, double[] code2) {
        return FaceApiMtfAndroid.getInstance().nativeCompareCodeD(code1, code2);
    }

}
