package com.hww.gdface_rz;

import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * @author hww
 * @date 2019-12-13.
 * email：
 * description：
 */
public class CameraUtil {

    private Camera camera;
    private Size previewSize;
    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Camera.PreviewCallback previewCallback;
    private int cameraId, degrees;

    public CameraUtil(int cameraId, int degrees, Size previewSize, SurfaceView surfaceView, Camera.PreviewCallback previewCallback) {
        this.previewCallback = previewCallback;
        this.surfaceView = surfaceView;
        this.previewSize = previewSize;
        this.cameraId = cameraId;
        this.degrees = degrees;
        surfaceHolder = this.surfaceView.getHolder();
        this.surfaceHolder.addCallback(callback);
    }

    public void openCamera() {
        if (null == camera) {
            try {
                camera = Camera.open(cameraId);
                mLog("获取摄像头对象成功");

                Camera.Parameters parameters = camera.getParameters();
                parameters.setPreviewSize(previewSize.getWidth(), previewSize.getHeight());
                camera.setParameters(parameters);

                camera.setDisplayOrientation(degrees);
                camera.setPreviewCallback(previewCallback);
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            mLog("camera对象不为空");
        }
    }

    public void closeCamera() {
        if (null != camera) {
            camera.setPreviewCallback(null);
            camera.stopPreview();
            camera = null;
        }

    }

    SurfaceHolder.Callback callback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            mLog("surfaceCreated");
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            mLog("surfaceChanged");
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            mLog("surfaceDestroyed");
        }
    };


    public static class Size {
        int width;
        int height;

        public Size(int width, int height) {
            this.width = width;
            this.height = height;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

    }

    private void mLog(String con) {
        Log.i("hww", "CameraUtil  " + con);
    }
}
