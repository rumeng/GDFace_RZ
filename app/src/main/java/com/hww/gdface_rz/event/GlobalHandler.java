package com.hww.gdface_rz.event;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.hww.gdface_rz.MyApplication;

import java.util.HashSet;
import java.util.Set;

public class GlobalHandler extends Handler {
    public GlobalHandler(Looper looper) {
        super(looper);
    }

    private Set<HandleMsgListener> listeners = new HashSet<>();

    public synchronized void addListener(HandleMsgListener listener) {
        try {
            listeners.add(listener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void removeListener(HandleMsgListener listener) {
        try {
            listeners.remove(listener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void informListener(Message msg) {
        for(HandleMsgListener item:listeners) {
            item.handleMsg(msg);
        }
    }

    @Override
    public void handleMessage(Message msg) {
        if(msg.obj!=null && msg.obj instanceof Runnable) {
            if(msg.arg1 == 1) {
                ((Runnable)msg.obj).run();
            } else {
                MyApplication.getApplication().executeInThreadPool((Runnable)msg.obj);
            }
            return;
        }
        informListener(msg);
    }

    public interface HandleMsgListener{
        void handleMsg(Message msg);
    }
}
