package com.hww.gdface_rz.event;

public class EventType {
    public static final int EXECUTE_RUNNABLE = 10000;//执行Runnable

    public static final int ZK_RESTART_REDERED = 400;//中控身份证读卡器重启
    public static final int ZK_CHECK_STATUS = 401;//中控身份证读卡器 检查当前状态
}
