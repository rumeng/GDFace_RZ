package com.hww.gdface_rz.db.entity;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

import java.util.Arrays;

/**
 * @author hww
 * @date 2020-01-03.
 * email：
 * description：
 */
@Entity
public class User {

    @Id(autoincrement = true)
    private Long id;
    private String name;
    private byte[] photo;
    private byte[] feature;

    private String idCard;
    private String validityTime;
    @Generated(hash = 733664003)
    public User(Long id, String name, byte[] photo, byte[] feature, String idCard,
            String validityTime) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.feature = feature;
        this.idCard = idCard;
        this.validityTime = validityTime;
    }
    @Generated(hash = 586692638)
    public User() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public byte[] getPhoto() {
        return this.photo;
    }
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
    public byte[] getFeature() {
        return this.feature;
    }
    public void setFeature(byte[] feature) {
        this.feature = feature;
    }
    public String getIdCard() {
        return this.idCard;
    }
    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    public String getValidityTime() {
        return this.validityTime;
    }
    public void setValidityTime(String validityTime) {
        this.validityTime = validityTime;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", photo=").append(Arrays.toString(photo));
        sb.append(", feature=").append(Arrays.toString(feature));
        sb.append(", idCard='").append(idCard).append('\'');
        sb.append(", validityTime='").append(validityTime).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

