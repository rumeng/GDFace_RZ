package com.hww.gdface_rz.db.utils;

import android.util.Log;

import com.hww.gdface_rz.db.UserDao;
import com.hww.gdface_rz.db.entity.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author hww
 * @date 2020-01-03.
 * email：
 * description：
 */
public class DBUserUtil {

    private static final DBUserUtil instance = new DBUserUtil();

    public static DBUserUtil getInstance() {
        return instance;
    }

    public List<User> getAllInfo() {
        return getUserDao().loadAll();
    }

    public List<User> findUserFromId(int id) {
        return getUserDao().queryBuilder().where(UserDao.Properties.Id.eq(id)).list();
    }

    public List<User> findUserFromIdCard(String idCard) {
        return getUserDao().queryBuilder().where(UserDao.Properties.IdCard.eq(idCard)).list();
    }

    /**
     * 新增数据
     *
     * @param entity
     */
    public void addUserBean(User entity) {
        long insert = getUserDao().insert(entity);
        getPersonListAdd(entity);
        Log.i("hww", "insert id = " + insert + "   " + entity.toString());
    }

    /**
     * 更新数据
     *
     * @param entity
     */
    public void updateUserBean(User entity, User oldUser) {
        getUserDao().update(entity);
        getPersonListUpdate(entity, oldUser);
    }

    /**
     * 删除数据
     *
     * @param entity
     */
    public void delUserBean(User entity) {
        getUserDao().delete(entity);
        getPersonListRemove(entity);
    }


    private UserDao getUserDao() {
        return DaoManager.getInstance().getDaoSession().getUserDao();
    }


    private ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();

    public Lock readLockPerson = reentrantReadWriteLock.readLock();
    private Lock writeLockPerson = reentrantReadWriteLock.writeLock();

    private Set<User> personList = new HashSet<>();

    /**
     * 查询设备上所有人员信息
     *
     * @return
     */
    public Set<User> getPersonList() {
        return personList;
    }

    public int getPersonListSize() {
        readLockPerson.lock();
        try {
            return personList.size();
        } finally {
            readLockPerson.unlock();
        }
    }

    public boolean getPersonListAdd(User entity) {
        writeLockPerson.lock();
        try {
            return personList.add(entity);
        } finally {
            writeLockPerson.unlock();
        }
    }

    public boolean getPersonListRemove(User person) {
        writeLockPerson.lock();
        try {
            return personList.remove(person);
        } finally {
            writeLockPerson.unlock();
        }
    }

    public void getPersonListUpdate(User user, User oldUser) {
        writeLockPerson.lock();
        try {
            personList.remove(oldUser);
            personList.add(user);
        } finally {
            writeLockPerson.unlock();
        }
    }

    public void getPersonListClear() {
        writeLockPerson.lock();
        try {
            personList.clear();
        } finally {
            writeLockPerson.unlock();
        }
    }

    public void addAllPersonList() {
        writeLockPerson.lock();
        try {
            List<User> allInfo = getAllInfo();
            if (null != allInfo && allInfo.size() > 0) {
                for (User user : allInfo) {
                    personList.add(user);
                }
            }
        } finally {
            writeLockPerson.unlock();
        }
    }


}
