package com.hww.gdface_rz;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hww.gdface_rz.faceutil.Face_Mtf;
import com.hww.gdface_rz.util.BGRDataInfo;
import com.hww.gdface_rz.util.ImageUtils;
import com.hww.gdface_rz.util.InfoShowIdCard;
import com.hww.gdface_rz.util.SharePreferencesUtils;
import com.hww.gdface_rz.util.SoundPoolUtils;
import com.hww.gdface_rz.util.ToolCameraData;
import com.hww.gdface_rz.view.FaceMarkSurface;
import com.hww.hjam.gl.FaceSurfaceView;
import com.hww.hjam.hww.InstanceHJAM;
import com.zkteco.android.IDReader.ZKInstance;
import com.zkteco.android.biometric.module.idcard.meta.IDCardInfo;

import net.gdface.mtfsdk.NativeFaceInfo;
import net.gdface.sdk.CodeInfo;


/**
 * @author hww
 * @date 2019-12-17.
 * email：
 * description：
 */
public class FragmentHJAM extends Fragment {
    private ZKInstance zkInstance;
    private int sumIDCard = 5;

    private SoundPoolUtils soundPoolUtils;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLog("FragmentHJAM onCreate()");
        zkInstance = new ZKInstance(getActivity(), readCardCallback);
        zkInstance.startReadIDCard();

        soundPoolUtils = new SoundPoolUtils(getActivity(), new int[]{R.raw.success, R.raw.fail});
    }

    private InstanceHJAM instanceHJAM;
    private FaceSurfaceView faceSurfaceView;
    private FaceMarkSurface markSurface;
    private TextView tvName, tvNum, tvTime, tvHint, tv_validityTime, tv_init_tip, tv_live, tv_face_title, tvDetectMore, tv_not_face;
    private ImageView ivIdCard, ivCamera;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewInflater = inflater.inflate(R.layout.fragment_hjam_double_facing, container, false);
        faceSurfaceView = viewInflater.findViewById(R.id.faceSurfaceView_hjam);
        markSurface = viewInflater.findViewById(R.id.faceMarkSurface_hjam);
        tvTime = viewInflater.findViewById(R.id.tvTime);
        tvTime.setVisibility(View.INVISIBLE);
        tvName = viewInflater.findViewById(R.id.tv_name);
        tvNum = viewInflater.findViewById(R.id.tv_id_card_num);
        tvHint = viewInflater.findViewById(R.id.tv_hint);
        ivIdCard = viewInflater.findViewById(R.id.iv_id_card);
        ivCamera = viewInflater.findViewById(R.id.iv_face);
        tv_validityTime = viewInflater.findViewById(R.id.tv_validityTime);
        tv_init_tip = viewInflater.findViewById(R.id.tv_init_tip);
        tv_live = viewInflater.findViewById(R.id.tv_live);
        tv_face_title = viewInflater.findViewById(R.id.tv_face_title);
        tvDetectMore = viewInflater.findViewById(R.id.tv_detect_more);
        tv_not_face = viewInflater.findViewById(R.id.tv_not_face_hint);

        instanceHJAM = new InstanceHJAM(getActivity(), faceSurfaceView, openState, markSurface);
        instanceHJAM.openCamera();

        if (instanceHJAM.isOpenLive()) {
            tv_live.setText("活体已启用");
        } else {
            tv_live.setText("活体已禁用");
        }

        tv_live.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                instanceHJAM.setOpenLive(!instanceHJAM.isOpenLive());
                SharePreferencesUtils.putBoolean(MyApplication.getApplication(), "isOpenLive", instanceHJAM.isOpenLive());
                if (instanceHJAM.isOpenLive()) {
                    tv_live.setText("活体已启用");
                } else {
                    tv_live.setText("活体已禁用");
                }
            }
        });

        if (instanceHJAM.isOpenDetectMore()) {
            tvDetectMore.setText(R.string.detect_close);
        } else {
            tvDetectMore.setText(R.string.detect_open);
        }

        tvDetectMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean detect = !instanceHJAM.isOpenDetectMore();
                instanceHJAM.setOpenDetect(detect);
                if (detect) {
                    instanceHJAM.setRzbdCallback(rzbdCallback);
                    tvDetectMore.setText(R.string.detect_close);
                } else {
                    tvDetectMore.setText(R.string.detect_open);
                }
            }
        });

        return viewInflater;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != instanceHJAM) {
            instanceHJAM.close();
        }
        if (null != zkInstance) {
            zkInstance.destroy();
            zkInstance = null;
        }
    }


    private String idCardNum = "";
    private long timeTag;

    private static final String path = "/sdcard/id.jpg";
    private com.zkteco.android.IDReader.ReadCardCallback readCardCallback = new com.zkteco.android.IDReader.ReadCardCallback() {
        @Override
        public void openSuccess() {
//            error_view.removeError(MainActivity.this,"zkReadCard");
        }

        @Override
        public void openFailed(int code) {

//            if(code == 0) {
//                error_view.removeError(MainActivity.this,"zkReadCard");
//
//            } else if(code == 1) {
//                ErrorInfo errorInfo = new ErrorInfo("zkReadCard");
//                errorInfo.setMsg(getString(R.string.idcard_device_error1));
//                error_view.addError(MainActivity.this,errorInfo.getTag(),errorInfo);
//
//            } else if(code == 2) {
//                ErrorInfo errorInfo = new ErrorInfo("zkReadCard");
//                errorInfo.setMsg(getString(R.string.idcard_device_error2));
//                error_view.addError(MainActivity.this,errorInfo.getTag(),errorInfo);
//            }
        }

        @Override
        public void readCardSuccess(IDCardInfo info, Bitmap headImg) {
            mLog("readCardSuccess  " + info.getName());

            if (instanceHJAM.isOpenDetectMore()) {
                return;
            }

            /*判断前后刷卡的是否为同一个人*/
            if (idCardNum.equals(info.getId())) {
//                handler.obtainMessage(LABEL_ID_CARD_TIMEKEEPING, "enter").sendToTarget();
            } else {
                idCardNum = info.getId();

                timeTag = System.currentTimeMillis();
                ImageUtils.save(headImg, path, Bitmap.CompressFormat.JPEG, false);
                Bitmap bitmap = ImageUtils.getBitmap(path);
                InfoShowIdCard infoShowIdCard = new InfoShowIdCard(info.getName(), info.getId(), info.getValidityTime(), bitmap);
                handler.obtainMessage(LABEL_SHOW_CARD_INFO, infoShowIdCard).sendToTarget();
                handler.obtainMessage(LABEL_ID_CARD_TIMEKEEPING, "enter").sendToTarget();

                CodeInfo[] codeInfos = null;
                double[] doubles = null;
                try {
                    long time1 = System.currentTimeMillis();
                    BGRDataInfo dataInfo = ToolCameraData.getCameraBGRDataInfo(bitmap);
                    NativeFaceInfo faceInfo = Face_Mtf.getInstance().faceDetect(dataInfo.getData(), dataInfo.getWidth(), dataInfo.getHeight(), 0);
                    //
                    if (null != faceInfo) {
                        doubles = Face_Mtf.getInstance().faceFeature(dataInfo.getData(), dataInfo.getWidth(), dataInfo.getHeight(), faceInfo);
                        mLog("身份证 特征提取用时：" + (System.currentTimeMillis() - time1));
                    }
                } catch (Exception e) {
                    mLog("detectAndGetCodeInfo " + e.toString());
                    e.printStackTrace();
                }
                if (null != doubles) {
                    instanceHJAM.rzbd(infoShowIdCard, doubles, rzbdCallback);
                } else {
                    idCardNum = "";
                }
            }
        }
    };

    InstanceHJAM.OpenState openState = new InstanceHJAM.OpenState() {
        @Override
        public void openSuccess() {
            instanceHJAM.startCameraThread();
//            handler.sendEmptyMessageDelayed(LABEL_INIT_RESULT,2000);
            handler.obtainMessage(LABEL_INIT_RESULT, null).sendToTarget();
        }

        @Override
        public void openFail(String fail) {
            mLog(fail);
            handler.obtainMessage(LABEL_INIT_RESULT, fail).sendToTarget();
        }
    };

    private InstanceHJAM.RzbdCallback rzbdCallback = new InstanceHJAM.RzbdCallback() {
        @Override
        public void success(InfoShowIdCard idCardInfo, Bitmap img, double score) {
            mLog("共用时：" + (System.currentTimeMillis() - timeTag));
            soundPoolUtils.playSound(R.raw.success);
            handler.obtainMessage(LABEL_SHOW_CARD_INFO, idCardInfo).sendToTarget();
            handler.obtainMessage(LABEL_SHOW_CAMERA_HEAD, img).sendToTarget();
            handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.rz_success)).sendToTarget();
            if (score == -100) {
                handler.obtainMessage(LABEL_SHOW_SCORE, "非活体").sendToTarget();
            } else {
                handler.obtainMessage(LABEL_SHOW_SCORE, String.format("%.3f", score /*+ 0.1d*/)).sendToTarget();
            }

        }

        @Override
        public void successMore(InfoShowIdCard idCardInfo) {
            soundPoolUtils.playSound(R.raw.success);
            /*多人脸比对成功结果*/
            handler.obtainMessage(LABEL_SHOW_CARD_INFO, idCardInfo).sendToTarget();
            handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.rz_success_more)).sendToTarget();
            handler.removeMessages(LABEL_ID_CARD_TIMEKEEPING);
            /*s识别成功后显示五秒*/
            timekeepingIdCard = 5;
            handler.sendEmptyMessageDelayed(LABEL_ID_CARD_TIMEKEEPING, 1000);
        }

        @Override
        public void failedMore() {
            handler.obtainMessage(LABEL_HINT_NOT_FACE, "fail").sendToTarget();
        }

        @Override
        public void failed(InfoShowIdCard idCardInfo, Bitmap img, double score) {
            soundPoolUtils.playSound(R.raw.fail);
            handler.obtainMessage(LABEL_SHOW_CARD_INFO, idCardInfo).sendToTarget();
            handler.obtainMessage(LABEL_SHOW_CAMERA_HEAD, img).sendToTarget();
            handler.obtainMessage(LABEL_SHOW_HINT, getActivity().getString(R.string.rz_failed)).sendToTarget();
            if (score == -100) {
                handler.obtainMessage(LABEL_SHOW_SCORE, "非活体").sendToTarget();
            } else {
                handler.obtainMessage(LABEL_SHOW_SCORE, String.format("%.3f", score)).sendToTarget();
            }
        }
    };

    /**
     * 刷卡成功后15秒之内需识别
     */
    private int timekeepingIdCard = 30;
    private static final int LABEL_ID_CARD_TIMEKEEPING = 0x80;
    private static final int LABEL_SHOW_CARD_INFO = 0x83;
    private static final int LABEL_SHOW_TOAST = 0x84;
    private static final int LABEL_SHOW_HINT = 0x85;
    private static final int LABEL_SHOW_CAMERA_HEAD = 0x86;
    private static final int LABEL_INIT_RESULT = 0x87;
    private static final int LABEL_SHOW_SCORE = 0x88;
    private static final int LABEL_HINT_NOT_FACE = 0x89;
    @SuppressLint("HandlerLeak")
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LABEL_ID_CARD_TIMEKEEPING:
                    if (null == msg.obj) {
                        if (timekeepingIdCard > 0) {
                            timekeepingIdCard--;
                            tvTime.setVisibility(View.VISIBLE);
                            tvTime.setText(String.valueOf(timekeepingIdCard));
                            if (timekeepingIdCard == 0) {
                                handler.obtainMessage(LABEL_ID_CARD_TIMEKEEPING, "sum").sendToTarget();
                            } else {
                                handler.sendEmptyMessageDelayed(LABEL_ID_CARD_TIMEKEEPING, 1000);
                            }
                        }
                    } else if (msg.obj.equals("enter")) {
                        timekeepingIdCard = 30;
                        handler.removeMessages(LABEL_ID_CARD_TIMEKEEPING);
                        tvTime.setVisibility(View.VISIBLE);
                        tvTime.setText(String.valueOf(timekeepingIdCard));
                        handler.sendEmptyMessageDelayed(LABEL_ID_CARD_TIMEKEEPING, 1000);
                    } else {
                        tvTime.setText(String.valueOf(0));
                        tvTime.setVisibility(View.GONE);
                        timekeepingIdCard = 30;
                        idCardNum = "";

                        handler.sendEmptyMessage(LABEL_SHOW_CARD_INFO);
                    }
                    break;
                case LABEL_SHOW_CARD_INFO:
                    Object objInfo = msg.obj;
                    if (null != objInfo && InfoShowIdCard.class.isInstance(objInfo)) {
                        InfoShowIdCard info = (InfoShowIdCard) objInfo;
                        String name = getString(R.string.show_name) + info.getName();
                        String id = getString(R.string.show_id_card_num) + info.getIdNum();
                        String avlidityTime = getString(R.string.show_validityTime) + info.getValidityTime();
                        tvName.setText(name);
                        tvNum.setText(id);
                        tv_validityTime.setText(avlidityTime);
                        ivIdCard.setImageBitmap(info.getBitmap());
                        tvHint.setText(getResources().getString(R.string.rzbd));
                    } else {
                        tvName.setText(getString(R.string.show_name));
                        tvNum.setText(getString(R.string.show_id_card_num));
                        tv_validityTime.setText(getString(R.string.show_validityTime));
                        ivIdCard.setImageResource(R.mipmap.idcard);
                        ivCamera.setImageResource(R.mipmap.face);
                        tv_face_title.setText("人脸采集");
                    }
                    break;
                case LABEL_SHOW_CAMERA_HEAD:
                    if (null != msg.obj) {
                        ivCamera.setImageBitmap((Bitmap) msg.obj);
                    } else {
                        ivCamera.setImageResource(R.mipmap.face);
                    }
                    break;
                case LABEL_SHOW_TOAST:
                    if (null != msg.obj) {
                        mToast("" + msg.obj);
                    }
                    break;

                case LABEL_SHOW_HINT:
                    Object objHint = msg.obj;
                    if (null != objHint) {
                        tvHint.setText("" + objHint);
                        if (timekeepingIdCard > 0) {
                            timekeepingIdCard = 3;
                        }
                    } else {
                        tvHint.setText(getString(R.string.rzbd));
                    }
                    break;
                case LABEL_INIT_RESULT:
                    if (msg.obj == null) {
                        tv_init_tip.setVisibility(View.GONE);
                    } else {
                        tv_init_tip.setVisibility(View.VISIBLE);
                        tv_init_tip.setText((String) msg.obj);
                    }

                    break;
                case LABEL_SHOW_SCORE:
                    tv_face_title.setText((String) msg.obj);
                    break;
                case LABEL_HINT_NOT_FACE:
                    if (null == msg.obj) {
                        tv_not_face.setVisibility(View.GONE);
                    } else {
                        handler.removeMessages(LABEL_HINT_NOT_FACE);
                        handler.sendEmptyMessageDelayed(LABEL_HINT_NOT_FACE, 3 * 1000);
                        tv_not_face.setVisibility(View.VISIBLE);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void mToast(String con) {
        Toast.makeText(getActivity(), con, Toast.LENGTH_SHORT).show();
    }

    private void mLog(String con) {
//        Log.i("hww", "FA-->" + con);
    }
}
