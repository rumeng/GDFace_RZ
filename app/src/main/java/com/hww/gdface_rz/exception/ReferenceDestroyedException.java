package com.hww.gdface_rz.exception;

//弱应用对象已经被销毁
public class ReferenceDestroyedException extends Exception {
    public ReferenceDestroyedException() {
        super();
    }

    public ReferenceDestroyedException(String message) {
        super(message);
    }
}
