package com.hww.gdface_rz;

import android.graphics.Bitmap;
import android.util.Log;

import com.hww.gdface_rz.db.entity.User;
import com.hww.gdface_rz.db.utils.DBUserUtil;
import com.hww.gdface_rz.util.DataSwitch;
import com.hww.gdface_rz.util.ImageUtils;
import com.hww.gdface_rz.util.InfoShowIdCard;

import java.util.List;

/**
 * @author hww
 * @date 2020-01-03.
 * email：
 * description：
 */
public class saveUser implements Runnable {
    private InfoShowIdCard idCardInfo;

    public saveUser(InfoShowIdCard idCardInfo) {
        this.idCardInfo = idCardInfo;
    }

    @Override
    public void run() {
        if (null != idCardInfo) {
            Log.i("hww", "idCardInfo.toString() ===> " + idCardInfo.toString());
            User user = new User();
            user.setIdCard(idCardInfo.getIdNum());
            user.setName(idCardInfo.getName());
            user.setValidityTime(idCardInfo.getValidityTime());
            double[] fd = idCardInfo.getFeature();
            if (null != fd && fd.length > 0) {
                byte[] bytes = DataSwitch.double2byte(fd, fd.length);
                user.setFeature(bytes);
            }
            Bitmap bitmap = idCardInfo.getBitmap();
            byte[] photo = ImageUtils.bitmap2Bytes(bitmap, Bitmap.CompressFormat.JPEG);
            user.setPhoto(photo);

            List<User> userFromIdCard = DBUserUtil.getInstance().findUserFromIdCard(idCardInfo.getIdNum());
            if (null != userFromIdCard && userFromIdCard.size() > 0) {
                User user1 = userFromIdCard.get(0);
                user.setId(user1.getId());
                Log.i("hww", "更新  用户 " + user1.toString());
                DBUserUtil.getInstance().updateUserBean(user, user1);
            } else {
                DBUserUtil.getInstance().addUserBean(user);
            }
        }
    }
}
