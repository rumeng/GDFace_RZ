package com.hww.gdface_rz.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import net.gdface.mtfsdk.MtfAndroidConfigProvider;
import net.gdface.mtfsdk.NativeFaceInfo;
import net.gdface.sdk.CodeInfo;
import net.gdface.sdk.FRect;

import java.util.ArrayList;
import java.util.List;

public class FaceMarkSurface extends SurfaceView implements SurfaceHolder.Callback {
    protected SurfaceHolder surfaceHolder;
    protected Context context;
    private int width, height;

    //人脸框画笔
    private Paint faceRectLinePaint;
    private int screenWidth;
    private int screenHeight;

    public FaceMarkSurface(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        getHolder().addCallback(this);
        setZOrderOnTop(true);
        setZOrderMediaOverlay(true);
        getHolder().setFormat(PixelFormat.TRANSPARENT);

        faceRectLinePaint = new Paint();
        faceRectLinePaint.setAntiAlias(true);
        faceRectLinePaint.setStrokeWidth((float) 4);
        faceRectLinePaint.setStyle(Paint.Style.STROKE);
        faceRectLinePaint.setColor(Color.RED);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        screenWidth = wm.getDefaultDisplay().getWidth();
        screenHeight = wm.getDefaultDisplay().getHeight();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceHolder = holder;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.i("hww", "surfaceChanged  =   width:" + width + "   height:" + height);
        this.width = width;
        this.height = height;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        surfaceHolder = null;
    }

    public boolean hasDrawInfo = false;

    public void clearDrawInfo() {
        if (surfaceHolder != null && hasDrawInfo) {
            hasDrawInfo = false;
            SurfaceHolder holder = surfaceHolder;
            try {
                Canvas canvas = holder.lockCanvas();
                if (canvas != null) {
                    canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                    holder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                e.printStackTrace();
                holder.unlockCanvasAndPost(null);
            } catch (Throwable th) {
                holder.unlockCanvasAndPost(null);
            }
        }
    }

    /**
     * 修改rect的参数(将算法返回的人脸框位置rect根据实际成像宽高进行调整，调整为适配实际成像rect数据)
     *
     * @param rect
     * @param width
     * @param height
     */
    private Rect transformRect(Rect rect, int width, int height) {
        Rect rectMod = new Rect();
        rectMod.left = rect.left * getWidth() / width;
        rectMod.right = rect.right * getWidth() / width;
        rectMod.top = rect.top * getHeight() / height;
        rectMod.bottom = rect.bottom * getHeight() / height;
        return rectMod;
    }

    /**
     * 把人脸检测的Rect，根据Surface的实际大小进行调整
     *
     * @param rect       人脸检测的Rect
     * @param dataWidth  图像的实际宽度
     * @param dataHeight 图像的实际高度
     * @return 调整后的新Rect
     */
    private Rect getFitRect(Rect rect, int dataWidth, int dataHeight) {
        if (rect == null) {
            return null;
        }
        Rect result = new Rect();
//        int width_5 = (rect.right - rect.left) / 6;//把人脸框 宽度增大1/6
        result.left = (rect.left * width / dataWidth);
        result.right = (rect.right * width / dataWidth);
        result.top = rect.top * height / dataHeight;
        result.bottom = rect.bottom * height / dataHeight;
        return result;
    }

    /**
     * 左右镜像
     *
     * @param rect   实际成像的正确人脸Rect数据
     * @param width  实际成像的宽
     * @param height 实际成像的高
     * @return
     */
    private Rect leftRightMirrorImage(Rect rect, int width, int height) {
        Rect rectMirror = new Rect();
        rectMirror.left = width - rect.right;
        rectMirror.right = width - rect.left;
        rectMirror.top = rect.top;
        rectMirror.bottom = rect.bottom;
        return rectMirror;
    }

    public void drawInfo(double[] detFace, int w, int h) {
        if (surfaceHolder == null) {
            return;
        }
        if (detFace == null) {
            return;
        }

        hasDrawInfo = true;
        SurfaceHolder holder = surfaceHolder;
        Canvas canvas = null;
        try {
            canvas = holder.lockCanvas();
            if (canvas != null) {
                canvas.drawColor(0, PorterDuff.Mode.CLEAR);

                List<DrawFaceInfo> listRect = getRectDrawFromDetect(detFace);
                //将屏幕人脸框转换为视频区域的人脸框
                for (int i = 0; i < listRect.size(); i++) {
                    DrawFaceInfo info = listRect.get(i);
                    Rect rect = info.getRect();
                    Rect rectMod = transformRect(rect, w, h);
                    DrawFaceInfo drawFaceInfo = new DrawFaceInfo();
                    drawFaceInfo.setRect(rectMod);
                    drawFaceInfo.setType(info.getType());
                    drawRect(canvas, drawFaceInfo);
                }
                holder.unlockCanvasAndPost(canvas);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (canvas != null) {
                holder.unlockCanvasAndPost(canvas);
            }
        } catch (Throwable th) {
            if (canvas != null) {
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void drawInfo(Rect rect, int width, int height) {
        if (surfaceHolder == null || rect == null) {
            return;
        }
        hasDrawInfo = true;
//        SurfaceHolder holder = surfaceHolder;
        Canvas canvas = null;
        try {
            canvas = surfaceHolder.lockCanvas();
            if (canvas != null) {
                synchronized (surfaceHolder) {
                    canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                    Rect rectMod = transformRect(rect, width, height);
                    drawFrame(canvas,rectMod);
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (Throwable th) {
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void drawInfo(List<CodeInfo> codeInfoList, boolean[] label, int width, int height) {
        if (surfaceHolder == null) {
            return;
        }
        if (codeInfoList == null) {
            return;
        }
        if (codeInfoList.size() == 0) {
            return;
        }

        if (null == label) {
            label = new boolean[]{false};
        }

        hasDrawInfo = true;
//        SurfaceHolder holder = surfaceHolder;
        Canvas canvas = null;
        try {
            canvas = surfaceHolder.lockCanvas();
            if (canvas != null) {
                synchronized (surfaceHolder) {
                    canvas.drawColor(0, PorterDuff.Mode.CLEAR);

                    List<DrawFaceInfo> listRect = getRectDrawFromCodeInfo(codeInfoList, label);
                    //将屏幕人脸框转换为视频区域的人脸框
                    for (int i = 0; i < listRect.size(); i++) {
                        DrawFaceInfo info = listRect.get(i);
                        Rect rect = info.getRect();
                        Rect rectMod = transformRect(rect, width, height);
                        DrawFaceInfo drawFaceInfo = new DrawFaceInfo();
                        drawFaceInfo.setRect(rectMod);
                        drawFaceInfo.setType(info.getType());
                        drawRect(canvas, drawFaceInfo);
                    }
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (Throwable th) {
            if (canvas != null) {
                surfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

    private void drawRect(Canvas canvas, DrawFaceInfo drawFaceInfo) {
        if (canvas == null) {
            return;
        }
        if (drawFaceInfo == null) {
            return;
        }

        if (drawFaceInfo.getType() == 0) {
            faceRectLinePaint.setColor(Color.GREEN);
        } else {
            faceRectLinePaint.setColor(Color.RED);
        }
        Rect rectSurface = drawFaceInfo.getRect();

        drawFrame(canvas, rectSurface);
    }

    /**
     * 绘制矩形框
     *
     * @param canvas
     * @param rect
     */
    private void drawFrame(Canvas canvas, Rect rect) {
        canvas.drawLine(rect.left > 0 ? rect.left : 0, rect.top > 0 ? rect.top : 0, rect.left, rect.bottom, faceRectLinePaint);
        canvas.drawLine(rect.left > 0 ? rect.left : 0, rect.top > 0 ? rect.top : 0, rect.right, rect.top, faceRectLinePaint);

        canvas.drawLine(rect.right < screenWidth ? rect.right : screenWidth, rect.bottom < screenHeight ? rect.bottom : screenHeight, rect.right, rect.top, faceRectLinePaint);
        canvas.drawLine(rect.right < screenWidth ? rect.right : screenWidth, rect.bottom < screenHeight ? rect.bottom : screenHeight, rect.left, rect.bottom, faceRectLinePaint);
    }

    /**
     * 绘制四个直角框
     *
     * @param canvas
     * @param rectSurface
     */
    private void drawFrameAngle(Canvas canvas, Rect rectSurface) {
        int lineLength = (rectSurface.right - rectSurface.left) / 4;
        //画人脸框
        canvas.drawLine(rectSurface.left, rectSurface.top, rectSurface.left + lineLength, rectSurface.top, faceRectLinePaint);
        canvas.drawLine(rectSurface.left, rectSurface.top, rectSurface.left, rectSurface.top + lineLength, faceRectLinePaint);

        canvas.drawLine(rectSurface.right - lineLength, rectSurface.top, rectSurface.right, rectSurface.top, faceRectLinePaint);
        canvas.drawLine(rectSurface.right, rectSurface.top, rectSurface.right, rectSurface.top + lineLength, faceRectLinePaint);

        canvas.drawLine(rectSurface.left, rectSurface.bottom - lineLength, rectSurface.left, rectSurface.bottom, faceRectLinePaint);
        canvas.drawLine(rectSurface.left, rectSurface.bottom, rectSurface.left + lineLength, rectSurface.bottom, faceRectLinePaint);

        canvas.drawLine(rectSurface.right, rectSurface.bottom - lineLength, rectSurface.right, rectSurface.bottom, faceRectLinePaint);
        canvas.drawLine(rectSurface.right - lineLength, rectSurface.bottom, rectSurface.right, rectSurface.bottom, faceRectLinePaint);
    }


    /**
     * 获取多人脸的Rect数据(将检测获取的人脸位置数据转换为画框需要的数据)
     *
     * @param rectarray
     * @return
     */
    public synchronized List<DrawFaceInfo> getRectDrawFromDetect(double[] rectarray) {
        List<DrawFaceInfo> rectList = new ArrayList<>();
        if (null == rectarray) {
            return rectList;
        }
        int faceCount = rectarray.length / MtfAndroidConfigProvider.FDDATA_LEN;
        for (int i = 0; i < faceCount; i++) {
            double[] faceInfo = new double[MtfAndroidConfigProvider.FDDATA_LEN];
            for (int j = 0; j < MtfAndroidConfigProvider.FDDATA_LEN; j++) {
                faceInfo[j] = rectarray[i * MtfAndroidConfigProvider.FDDATA_LEN + j];
            }

            int d1 = (int) (faceInfo[0]);
            int d2 = (int) (faceInfo[1]);
            int d3 = (int) (faceInfo[2]);
            int d4 = (int) (faceInfo[3]);
            int left = d1;
            int top = d2;
            int right = d1 + d3;
            int bottom = d2 + d4;
            Rect rect1 = new Rect(left + ((int) faceInfo[faceInfo.length - 3]), top + ((int) faceInfo[faceInfo.length - 2]), right + ((int) faceInfo[faceInfo.length - 3]), bottom + ((int) faceInfo[faceInfo.length - 2]));
            DrawFaceInfo info1 = new DrawFaceInfo();
            info1.setRect(rect1);
            info1.setSource(faceInfo[14]);
            info1.setType(getIdentifyResult(faceInfo));
            rectList.add(info1);
        }
        return rectList;
    }

    /**
     * 获取多人脸的Rect数据(将检测获取的人脸位置数据转换为画框需要的数据)
     *
     * @param codeInfoList
     * @return
     */
    public synchronized List<DrawFaceInfo> getRectDrawFromCodeInfo(List<CodeInfo> codeInfoList, boolean[] label) {
        List<DrawFaceInfo> rectList = new ArrayList<>();
        if (null == codeInfoList) {
            return rectList;
        }
        int faceCount = codeInfoList.size();
        for (int i = 0; i < faceCount; i++) {
            NativeFaceInfo info = (NativeFaceInfo) codeInfoList.get(i);
            FRect pos = info.getPos();
            int left = pos.getLeft();
            int top = pos.getTop();
            int right = left + pos.getWidth();
            int bottom = top + pos.getHeight();
//            Rect rect1 = new Rect(left + ((int) faceInfo[faceInfo.length - 3]), top + ((int) faceInfo[faceInfo.length - 2]), right + ((int) faceInfo[faceInfo.length - 3]), bottom + ((int) faceInfo[faceInfo.length - 2]));
            Rect rect = new Rect(left, top, right, bottom);
            DrawFaceInfo info1 = new DrawFaceInfo();
            info1.setRect(rect);
            info1.setSource(info.getConfidence());
            if (label[i]) {
                info1.setType(1);
            } else {
                info1.setType(0);
            }
            rectList.add(info1);
        }
        return rectList;
    }

    /**
     * 返回识别标记结果     0为未识别       1为已识别
     *
     * @param detectFace
     * @return
     */
    public synchronized int getIdentifyResult(double[] detectFace) {
        if (null != detectFace && MtfAndroidConfigProvider.FDDATA_LEN <= detectFace.length) {
            if (detectFace[MtfAndroidConfigProvider.FDDATA_LEN - 1] == 1) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}
