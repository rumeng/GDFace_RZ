package com.hww.gdface_rz.view;

import android.graphics.Rect;

public class DrawFaceInfo {

    private Rect rect;
    private int type;//0为未检测的人脸标记，1为已经检测成功的人脸标记
    private double source;//分数

    public Rect getRect() {
        return rect;
    }

    public void setRect(Rect rect) {
        this.rect = rect;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getSource() {
        return source;
    }

    public void setSource(double source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "DrawFaceInfo{" +
                "rect=" + rect +
                ", type=" + type +
                ", source=" + source +
                '}';
    }
}
