package com.hww.gdface_rz.util;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hww
 * @date 2020-01-04.
 * email：
 * description：
 */
public class JudgeTool {
    private static JudgeTool judgeTool;

    public static JudgeTool getInstance() {
        if (null == judgeTool) {
            synchronized (JudgeTool.class) {
                if (null == judgeTool) {
                    judgeTool = new JudgeTool();
                }
            }
        }
        return judgeTool;
    }

    private final ConcurrentHashMap<String, Long> logIntervalHashMap = new ConcurrentHashMap<>();

    private static final long time = 3000;

    /**
     * 在缓存中计算对应personId能否在指定识别间隔内通过
     *
     * @param idCard
     * @param detectTime
     * @return
     */
    public synchronized boolean judgeDetectIntervalTime(String idCard, long detectTime) {

        Long oldTime = logIntervalHashMap.put(idCard, detectTime);
        if (null == oldTime) {
            /*put返回null，该personId未在map里面*/
            return true;
        } else {
            /*put返回指定内容，该personId已经识别过，需要判断时间点*/
            synchronized (logIntervalHashMap) {
                for (Iterator<Map.Entry<String, Long>> iterator = logIntervalHashMap.entrySet().iterator(); iterator.hasNext(); ) {
                    Map.Entry<String, Long> mapIterator = iterator.next();
                    if (!mapIterator.getKey().equals(idCard)) {
                        /*验证不是当personId对象，识别之后3次识别间隔时间未重复识别，则默认删除对应的缓存记录*/
                        boolean delete = timeCount(mapIterator.getValue(), detectTime, time);
                        if (delete) {
                            iterator.remove();
                        }
                    }
                }

                boolean interval = timeCount(oldTime, detectTime, time);
                if (interval) {
                    return true;
                } else {
                    logIntervalHashMap.put(idCard, oldTime);
                    return false;
                }
            }
        }
    }

    /**
     * 判断end跟start时间差是否大于log
     *
     * @param start 开始时间
     * @param end   结束时间
     * @param log   比对时间
     * @return true(end - start > log)     false(end-start<log)
     */
    private synchronized boolean timeCount(long start, long end, long log) {
        if ((end - start) >= log) {
            return true;
        } else {
            return false;
        }
    }
}
