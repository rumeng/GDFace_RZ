package com.hww.gdface_rz.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import java.util.HashMap;
import java.util.Map;

public class SoundPoolUtils {
    //SoundPool可以装载多个音频资源，但是最大只能申请1MB的内存空间
    private SoundPool pool;
    private Map<Integer,Integer> poolMap;
    private boolean isOpenSound = true;

    public SoundPoolUtils(Context context, int[] rawResources) {
        if(rawResources.length==0) return;
        //第一个参数为音频池最多支持装载多少个音频，就是音频池的大小；第二个参数指定声音的类型，在AudioManager类中以常量的形式定义，一般指定为AudioManager.STREAM_MUSIC即可；第三个参数为音频的质量，默认为0
        pool = new SoundPool(rawResources.length, AudioManager.STREAM_MUSIC, 0);
        poolMap = new HashMap<Integer,Integer>();
        for(int i=0;i<rawResources.length;i++) {
            poolMap.put(rawResources[i],pool.load(context, rawResources[i], 1));
        }
    }

    public void playSound(int rawResource) {
        if(!isOpenSound) return;
        if(pool!=null) {
            //soundID参数为资源ID；leftVolume和rightVolume个参数为左右声道的音量，从大到小取0.0f~1.0f之间的值；priority为音频质量，暂时没有实际意义，传0即可；loop为循环次数，0为播放一次，-1为无线循环，其他正数+1为播放次数，如传递3，循环播放4次；rate为播放速率，从大到小取0.0f~2.0f，1.0f为正常速率播放。
            pool.play(poolMap.get(rawResource), 1.0f, 1.0f, 0, 0, 1.0f);
        }
    }

    public void distory() {
        if (pool != null) {
            pool.release();
            pool = null;
        }
    }
}
