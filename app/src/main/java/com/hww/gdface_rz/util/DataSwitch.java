package com.hww.gdface_rz.util;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;

/**
 * Created by admin on 2018/6/4.
 * <p>
 * 将特征码double类型转换为byte类型
 */

public class DataSwitch {

    public static double[] byte2double(byte[] bs) {
        if (null == bs) {
            return null;
        }
        ByteBuffer by = ByteBuffer.wrap(bs);
        DoubleBuffer dBuffer = by.asDoubleBuffer();
        double[] dst = new double[dBuffer.remaining()];
        dBuffer.get(dst);
        return dst;
    }

    public static byte[] double2byte(double[] doub, int size) {
        if (null == doub) {
            return null;
        }
        ByteBuffer byteBuffer = ByteBuffer.allocate(size * 8);
        for (double d : doub) {
            byteBuffer.putDouble(d);
        }
        byte[] bs = byteBuffer.array();
        return bs;
    }


    /**
     * String 转  byte[]
     * @param byteArray
     * @return
     */
    public static String byteArrayToStr(byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }
        String str = new String(byteArray);
        return str;
    }


    /**
     *  byte[] 转 String
     * @param str
     * @return
     */
    public static byte[] strToByteArray(String str) {
        if (str == null) {
            return null;
        }
        byte[] byteArray = str.getBytes();
        return byteArray;
    }

}
