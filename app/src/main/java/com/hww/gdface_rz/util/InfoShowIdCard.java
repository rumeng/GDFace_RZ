package com.hww.gdface_rz.util;

import android.graphics.Bitmap;

import java.util.Arrays;

/**
 * @author hww
 * @date 2019-12-17.
 * email：
 * description：
 */
public class InfoShowIdCard {

    private String name;
    private String idNum;
    private String validityTime;
    private Bitmap bitmap;
    private double[] feature;

    public InfoShowIdCard(String name, String idNum, String validityTime, Bitmap bitmap) {
        this.name = name;
        this.idNum = idNum;
        this.validityTime = validityTime;
        this.bitmap = bitmap;
    }

    public String getName() {
        return name;
    }

    public String getIdNum() {
        return idNum;
    }

    public String getValidityTime() {
        return validityTime;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public double[] getFeature() {
        return feature;
    }

    public void setFeature(double[] feature) {
        this.feature = feature;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InfoShowIdCard{");
        sb.append("name='").append(name).append('\'');
        sb.append(", idNum='").append(idNum).append('\'');
        sb.append(", validityTime='").append(validityTime).append('\'');
        sb.append(", bitmap=").append((null == bitmap) ? "null" : "not null");
        sb.append(", feature=").append(null == feature ? "null" : feature.length);
        sb.append('}');
        return sb.toString();
    }
}
