package com.hww.gdface_rz.util;

/**
 * @author hww
 * @date 2018-12-07.
 * GitHub：
 * email：
 * description：
 */
public class BGRDataInfo {

    private byte[] data;
    private int width;
    private int height;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "BGRDataInfo{" +
                "data.length=" + data.length +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
