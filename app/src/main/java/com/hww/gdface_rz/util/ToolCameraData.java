package com.hww.gdface_rz.util;

import android.graphics.Bitmap;
import android.util.Log;

import java.nio.ByteBuffer;

import gu.jimgutil.MatrixUtils;

public class ToolCameraData {

    private static ThreadLocal<ByteBuffer> byteBufferThreadLocal = new ThreadLocal<>();
    private long timeData = 0;

    /**
     * 返回算法需要的各种数据
     *
     * @param bitmap
     * @return
     */
    public static synchronized BGRDataInfo getCameraBGRDataInfo(Bitmap bitmap) {
        /*timeData = System.currentTimeMillis();*/
        BGRDataInfo info = new BGRDataInfo();

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        try {
            byte[] data_BGR = new byte[width * height * 3];
            int bytes = bitmap.getByteCount();
            if (null == byteBufferThreadLocal.get() || byteBufferThreadLocal.get().capacity() != bytes) {
                byteBufferThreadLocal.set(ByteBuffer.allocate(bytes));
            }
            bitmap.copyPixelsToBuffer(byteBufferThreadLocal.get());
            byte[] temp = byteBufferThreadLocal.get().array();

            //stride 每行步长(字节长度),RGBA(width*4)  BGR(width*3)
            MatrixUtils.RGBA2BGR(temp, width, height, width * 4, data_BGR);
            info.setData(data_BGR);
            info.setWidth(width);
            info.setHeight(height);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("hww", "getCameraBGRDataInfo== " + e.toString());
        } finally {
            /*Info_Date.getInstance().mLog("getCameraBGRDataInfo == " + (System.currentTimeMillis() - timeData));*/
            byteBufferThreadLocal.get().clear();
        }
        return info;
    }
}
