package com.hww.gdface_rz.util;


import com.hww.gdface_rz.exception.ReferenceDestroyedException;

import java.lang.ref.WeakReference;

public class WeakRefRunnable<T> implements Runnable {
    private WeakReference<T> wf;

    public WeakRefRunnable(T context) {
        wf = new WeakReference<T>(context);
    }

    @Override
    public void run() {
        try {
            doWork();
        } catch (ReferenceDestroyedException e) {
            e.printStackTrace();
        }
    }

    public void doWork() throws ReferenceDestroyedException {

    }

    public T getContext() throws ReferenceDestroyedException {
        T context = wf.get();
        if (context==null) throw new ReferenceDestroyedException();
        return context;
    }
}
