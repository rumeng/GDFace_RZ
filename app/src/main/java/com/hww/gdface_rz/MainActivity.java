package com.hww.gdface_rz;

import android.Manifest;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hww.gdface_rz.db.utils.DBUserUtil;
import com.hww.gdface_rz.faceutil.Face_Mtf;
import com.hww.gdface_rz.util.SharePreferencesUtils;
import com.hww.mtf.MTFRegister;
import com.permission.AnnPermission;
import com.permission.PermissionCallback;
import com.permission.PermissionItem;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private LinearLayout ll_auth;
    private TextView popNagative, popPositive, tv_init_SDK;
    private EditText popEdittext;

    private boolean permissionFinish = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ll_auth = findViewById(R.id.ll_auth);
        popEdittext = findViewById(R.id.pop_txt_dialog_tip);
        popNagative = findViewById(R.id.pop_btn_selectNegative);
        popPositive = findViewById(R.id.pop_btn_selectPositive);
        tv_init_SDK = findViewById(R.id.tv_init_SDK);

        popNagative.setOnClickListener(clickListener);

        List<PermissionItem> permissions = new ArrayList<PermissionItem>();
        permissions.add(new PermissionItem(Manifest.permission.CAMERA, getString(R.string.permission_camera), R.drawable.permission_ic_camera));
        permissions.add(new PermissionItem(Manifest.permission.WRITE_EXTERNAL_STORAGE, getString(R.string.permission_storage), R.drawable.permission_ic_storage));
        AnnPermission.create(MainActivity.this).permissions(permissions).animStyle(R.style.PermissionAnimFade)
                .checkMutiPermission(new PermissionCallback() {
                    @Override
                    public void onClose() {
                        mLog("onClose  ");
                    }

                    @Override
                    public void onFinish() {
                        permissionFinish = true;
                        if (permissionFinish && Face_Mtf.getInstance().faceLicenseValid()) {
                            getSupportFragmentManager().beginTransaction().add(R.id.main, new FragmentHJAM()).commit();
                            mLog("onFinish  ");
                            hideSoftInput(popEdittext);
                        }
                        /*getSupportFragmentManager().beginTransaction().add(R.id.main, new DoubleFacingFragment()).commit();*/
                    }

                    @Override
                    public void onDeny(String permission, int position) {
                        mLog(String.format("onDeny  permission=%s   position=%s", permission, position));
                    }

                    @Override
                    public void onGuarantee(String permission, int position) {
                        mLog(String.format("onGuarantee  permission=%s   position=%s", permission, position));
                    }
                });

        String key = SharePreferencesUtils.getString(this, MTFRegister.LICENSE_KEY);
        if (key != null && key.length() > 0) {
            initMTFSDK(key);
        } else {
            ll_auth.setVisibility(View.VISIBLE);
            popPositive.setOnClickListener(clickListener);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        DBUserUtil.getInstance().addAllPersonList();
        Log.e("hww", "init user size = " + DBUserUtil.getInstance().getPersonListSize());
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.pop_btn_selectNegative:
                    finish();
                    break;
                case R.id.pop_btn_selectPositive:
                    //授权
                    String license = popEdittext.getText().toString().trim();
                    if (null != license && license.length() > 0) {
                        initMTFSDK(license);
                    } else {
                        Toast.makeText(MainActivity.this, "授权码不能为空", Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private boolean isRuning = false;

    private void initMTFSDK(final String licenseKey) {
        if (isRuning) return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                isRuning = true;
                final MTFRegister.MsgOnline online = Face_Mtf.getInstance().faceRegister(MainActivity.this, licenseKey);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (online.isState()) {
                            ll_auth.setVisibility(View.GONE);
                            if (permissionFinish && Face_Mtf.getInstance().faceLicenseValid()) {
                                getSupportFragmentManager().beginTransaction().add(R.id.main, new FragmentHJAM()).commit();
                                mLog("onFinish  ");
                                hideSoftInput(popEdittext);
                            }

                        } else {
                            tv_init_SDK.setTextColor(Color.RED);
                            tv_init_SDK.setText("算法授权失败");

                            ll_auth.setVisibility(View.VISIBLE);
                            popPositive.setOnClickListener(clickListener);
                        }
                    }
                });

                isRuning = false;
            }
        }).start();
    }

    /**
     * 隐藏软键盘
     *
     * @param editText
     */
    private void hideSoftInput(EditText editText) {
        InputMethodManager inputMethodManager = (InputMethodManager) editText.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken()
                , InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //-------------------关闭-------------------------------------
    private long exitTime = 0;// 计算短时间内点击返回按钮的次数

    /**
     * 点击返回按钮2次，退出应用程序
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                Toast.makeText(MainActivity.this, "再点一次，退出应用程序", Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else {
                onDestroy();
                System.exit(0);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void mLog(String con) {
        Log.e("hww", "" + con);
    }
}
