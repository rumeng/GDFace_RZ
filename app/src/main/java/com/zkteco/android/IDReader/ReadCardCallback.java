package com.zkteco.android.IDReader;

import android.graphics.Bitmap;

import com.zkteco.android.biometric.module.idcard.meta.IDCardInfo;

public interface ReadCardCallback {
    public void openSuccess();
    /**
     * 连接身份证读卡器失败
     * @param code 0:没有检测到身份证读卡器  1：连接身份证读卡器失败  3：读取身份证读卡器失败
     * */
    public void openFailed(int code);
    public void readCardSuccess(IDCardInfo idCardInfo, Bitmap headImg);
}
