package com.zkteco.android.IDReader;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.hww.gdface_rz.MyApplication;
import com.hww.gdface_rz.R;
import com.hww.gdface_rz.event.EventType;
import com.hww.gdface_rz.exception.ReferenceDestroyedException;
import com.hww.gdface_rz.util.SoundPoolUtils;
import com.hww.gdface_rz.util.WeakRefRunnable;
import com.hww.gdface_rz.util.WeakRefThread;
import com.zkteco.android.biometric.core.device.ParameterHelper;
import com.zkteco.android.biometric.core.device.TransportType;
import com.zkteco.android.biometric.core.utils.LogHelper;
import com.zkteco.android.biometric.module.idcard.IDCardReader;
import com.zkteco.android.biometric.module.idcard.IDCardReaderFactory;
import com.zkteco.android.biometric.module.idcard.exception.IDCardReaderException;
import com.zkteco.android.biometric.module.idcard.meta.IDCardInfo;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class ZKInstance {
    private static final int VID = 1024;    //IDR VID
    private static final int PID = 50010;     //IDR PID
    public IDCardReader idCardReader = null;
    private ReadCardThread readCardThread;

    private WeakReference<Context> mContextWR;
    private ReadCardCallback cardCallback;
    private BroadcastReceiver usbBroadcastReceiver = null;
    private UsbManager musbManager;

    private SoundPoolUtils soundPoolUtils;

    public ZKInstance(Context context, ReadCardCallback cardCallback) {
        mContextWR = new WeakReference<Context>(context);
        this.cardCallback = cardCallback;

        LogHelper.setLevel(8);//不要打印日志

        //注册USB广播
        registerReceiver();
    }

    /**
     * 中控身份证读卡是否正在运行
     * */
    public boolean isRuning() {
        if(readCardThread==null) return false;
        return true;
    }

    private final Object LOCK_START = new Object();
    /**
     * 开始身份证读卡
     * */
    public boolean startReadIDCard() {
        synchronized (LOCK_START) {
            //已启动
            if(isRuning()) return true;

            //查找USB身份证读卡器
            UsbDevice zkDevice = getZKDevice();
            if(zkDevice == null) {
                if(cardCallback!=null) cardCallback.openFailed(0);
                return false;
            }
            MyApplication.getApplication().removeMsg(EventType.ZK_CHECK_STATUS);

            //是否有权限
            if(!haveUsbPermission(zkDevice,true)){
                restartLater();
                return false;
            }

            if(idCardReader == null){
                Map idrparams = new HashMap();
                idrparams.put(ParameterHelper.PARAM_KEY_VID, VID);
                idrparams.put(ParameterHelper.PARAM_KEY_PID, PID);
                idCardReader = IDCardReaderFactory.createIDCardReader(mContextWR.get(), TransportType.USB, idrparams);
            }

            try {
                mLog("ZKInstance==>startReadIDCard()");
                idCardReader.open(0);
            } catch (IDCardReaderException e) {
                mLog("ZKInstance==>startReadIDCard()：error:"+e.getMessage());
                stopReadIDCard();
                restartLater();
                return false;
            }

            restartSum = 0;
            readCardThread = new ReadCardThread(this);
            readCardThread.start();

            if(soundPoolUtils==null) soundPoolUtils = new SoundPoolUtils(mContextWR.get(),new int[]{R.raw.din});

            if(cardCallback!=null) cardCallback.openSuccess();

            mLog("ZKInstance==>startReadIDCard() SUCCESS");
            return true;
        }
    }


    //1.5秒后尝试重新启动读卡器
    private int restartSum = 0;
    private final Object LOCK_RESTART = new Object();
    private void restartLater() {
        synchronized (LOCK_RESTART) {
            restartSum++;
            if(restartSum<=4) {
                MyApplication.getApplication().executeRunnableDelayed(EventType.ZK_RESTART_REDERED,restartRunnable,1500);
            } else {
                restartSum = 0;
                MyApplication.getApplication().removeMsg(EventType.ZK_CHECK_STATUS);
                MyApplication.getApplication().removeMsg(EventType.ZK_RESTART_REDERED);
                if(cardCallback!=null) cardCallback.openFailed(1);
            }
        }
    }

    private RestartRunnable restartRunnable = new RestartRunnable(this);//重启身份证阅读器
    private static class RestartRunnable extends WeakRefRunnable<ZKInstance> {
        public RestartRunnable(ZKInstance context) {
            super(context);
        }

        @Override
        public void doWork() throws ReferenceDestroyedException {
            getContext().startReadIDCard();
        }
    };

    /**
     * 查找中控USB身份证读卡器
     * @return
     * */
    private UsbDevice getZKDevice() {
        Context mContext = mContextWR.get();
        if(mContext == null) return null;

        musbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        for (UsbDevice device : musbManager.getDeviceList().values()) {
            if (device.getVendorId() == VID && device.getProductId() == PID){
                mLog("ZKInstance==>USB：检测到身份证阅读器");
                return device;
            }
        }
        mLog("ZKInstance==>USB：没有身份证阅读器");
        return null;
    }

    /**
     * 获取USB读取权限
     * @param device USB设备
     * @param toRequestPermission 如果没有权限，是否去申请权限
     * */
    private boolean haveUsbPermission(UsbDevice device, boolean toRequestPermission) {
        Context mContext = mContextWR.get();
        if(mContext == null) return false;

        if(musbManager==null) musbManager = (UsbManager) mContext.getSystemService(Context.USB_SERVICE);
        if(!musbManager.hasPermission(device)) {
            mLog("ZKInstance==>USB:没有权限");
            if(toRequestPermission) {
                Intent intent = new Intent(INTENT_USB_PERMISSION);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
                musbManager.requestPermission(device, pendingIntent);
            }
            return false;
        } else {
            mLog("ZKInstance==>USB:有权限");
            return true;
        }
    }

    //注册USB广播
    private void registerReceiver() {
        Context mContext = mContextWR.get();
        usbBroadcastReceiver = new ZKUsbBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
        intentFilter.addAction(INTENT_USB_PERMISSION);
        if(mContext!=null) mContext.registerReceiver(usbBroadcastReceiver, intentFilter);
    }

    private final String INTENT_USB_PERMISSION = "com.example.scarx.idcardreader.USB_PERMISSION";
    class ZKUsbBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            switch (action) {
                case INTENT_USB_PERMISSION://接受到自定义广播:设备获取权限结果
                    UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    //获取到权限
                    if(haveUsbPermission(usbDevice,false)){
                        if(!isRuning()) startReadIDCard();
                    }
                    break;
                case UsbManager.ACTION_USB_DEVICE_ATTACHED://接收到USB插入广播
                    //查找中控USB身份证读卡器
                    UsbDevice zkDevice = getZKDevice();
                    if(zkDevice != null){
                        MyApplication.getApplication().removeMsg(EventType.ZK_CHECK_STATUS);
                        if(haveUsbPermission(zkDevice,true)){
                            if(!isRuning()) startReadIDCard();
                        }
                    }
                    break;
                case UsbManager.ACTION_USB_DEVICE_DETACHED://接收到USB拔出广播
//                    UsbDevice device_remove = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if(getZKDevice() == null) {
                        stopReadIDCard();
                    }
                    break;
            }
        }
    }

    private static class ChcekStatus extends WeakRefRunnable<ZKInstance>{
        public ChcekStatus(ZKInstance context) {
            super(context);
        }

        @Override
        public void doWork() throws ReferenceDestroyedException {
            UsbDevice zkDevice = getContext().getZKDevice();
            if(zkDevice == null) {
                if(getContext().cardCallback!=null) getContext().cardCallback.openFailed(0);
            } else {
                if(getContext().cardCallback!=null) getContext().cardCallback.openFailed(2);
            }
        }
    }

    private static class ReadCardThread extends WeakRefThread<ZKInstance> {
        private boolean stopRun = false;

        public ReadCardThread(ZKInstance context) {
            super(context);
        }

        public void stopThread() {
            stopRun = true;
            this.interrupt();
        }

        @Override
        public void doWork() throws ReferenceDestroyedException {
            work();
            getContext().stopReadIDCard();
        }

        private void work() throws ReferenceDestroyedException {
            mLog("ZKInstance==>ReadCardThread RUN");

            boolean ret = false;
            IDCardInfo idCardInfo = null;
            long nTickstart = 0;
            int errorSum = 0;
            while (!stopRun) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e1) {
                }

                if(getContext().idCardReader==null) return;

                try {
                    //getStatus：获取设备状态，true：正常，false：异常
                    ret = getContext().idCardReader.getStatus(0);
                } catch (IDCardReaderException e) {
                    mLog("ZKInstance==> IDCardReaderException ErrorCode:"+e.getErrorCode()+" InternalErrorCode:"+e.getInternalErrorCode()+" error:"+e.getMessage());

                    errorSum++;
                    if(errorSum>20) {
                        mLog("ZKInstance==>ReadCardThread 读取设备错误");
                        stopRun = true;
                        MyApplication.getApplication().removeMsg(EventType.ZK_CHECK_STATUS);
                        MyApplication.getApplication().executeRunnableOnMainDelayed(EventType.ZK_CHECK_STATUS,new ChcekStatus(getContext()),0);
                        return;
                    }
                }

                if (!ret){
                    if(getContext().idCardReader==null) return;
                    try {
                        //reset:重置设备
                        getContext().idCardReader.reset(0);
                    } catch (IDCardReaderException e) {
                        mLog("ZKInstance==> IDCardReaderException ErrorCode:"+e.getErrorCode()+" InternalErrorCode:"+e.getInternalErrorCode()+" error:"+e.getMessage());
                    }
                }

                ret = false;
                if(getContext().idCardReader==null) return;
                try {
                    //findCard：寻卡 true：成功  false：失败
                    if(getContext().idCardReader.findCard(0)) {
                        //selectCard：选卡
                        getContext().idCardReader.selectCard(0);

                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e1) {
                        }

                        nTickstart = System.currentTimeMillis();
                        if(idCardInfo == null) idCardInfo = new IDCardInfo();
                        //读卡
                        if(getContext().idCardReader==null) return;
                        ret = getContext().idCardReader.readCard(0, 0, idCardInfo);
                    }

                } catch (IDCardReaderException e) {
                    continue;
                }

                if (ret) {
                    errorSum = 0;
                    nTickstart = System.currentTimeMillis() - nTickstart;
                    mLog( "ZKInstance==> idCard 读卡成功，耗时：" + nTickstart + "毫秒");
                    if (idCardInfo.getPhotolength() > 0) {
                        nTickstart = System.currentTimeMillis();
                        byte[] buf = new byte[WLTService.imgLength];
                        if (1 == WLTService.wlt2Bmp(idCardInfo.getPhoto(), buf)) {
                            Bitmap bitmap = IDPhotoHelper.Bgr2Bitmap(buf);
                            nTickstart = System.currentTimeMillis() - nTickstart;
                            mLog( "ZKInstance==> idCard 图片解码成功，耗时：" + nTickstart + "毫秒");
                            if(getContext().soundPoolUtils!=null) getContext().soundPoolUtils.playSound(R.raw.din);
                            if(getContext().cardCallback!=null) getContext().cardCallback.readCardSuccess(idCardInfo,bitmap);
                        }
                    }

                    idCardInfo = new IDCardInfo();
                }
            }
        }

        private void mLog(String con) {
            Log.e("hww", "" + con);
        }
    }

    /**
     * 停止
     * */
    public void stopReadIDCard() {
        if(idCardReader!=null) {
            try {
                idCardReader.close(0);
            } catch (IDCardReaderException e) {
                e.printStackTrace();
            }
        }
        if(readCardThread!=null) {
            readCardThread.stopThread();
            readCardThread = null;
        }
    }

    /**
     * 销毁
     * */
    public void destroy() {
        Context mContext = mContextWR.get();
        if(usbBroadcastReceiver!=null && mContext != null){
            mContext.unregisterReceiver(usbBroadcastReceiver);
            usbBroadcastReceiver = null;
        }

        MyApplication.getApplication().removeMsg(EventType.ZK_CHECK_STATUS);
        MyApplication.getApplication().removeMsg(EventType.ZK_RESTART_REDERED);
        stopReadIDCard();

        if(idCardReader!=null) {
            IDCardReaderFactory.destroy(idCardReader);
            idCardReader = null;
        }
        mContextWR = null;
        cardCallback = null;
    }

    private void mLog(String con) {
        Log.e("hww", "" + con);
    }
}
