package com.permission;

import java.io.Serializable;

/**
 * Created by Com_PC on 2017/5/10 0010.
 *
 * @author Com_PC
 */
public class PermissionItem implements Serializable {
    public String permissionName;
    public String permission;
    public int permissionIconRes;

    public PermissionItem(String permission, String permissionName, int permissionIconRes) {
        this.permission = permission;
        this.permissionName = permissionName;
        this.permissionIconRes = permissionIconRes;
    }

    public PermissionItem(String permission) {
        this.permission = permission;
    }
}
