package com.permission;

import java.io.Serializable;

/**
 * Created by Com_PC on 2017/5/10 0010.
 *
 * @author Com_PC
 */

public interface PermissionCallback extends Serializable {
    /**
     * 用户关闭授权
     */
    void onClose();

    /**
     * 权限申请完成
     */
    void onFinish();

    /**
     * 拒绝权限
     *
     * @param permission 被拒绝权限
     * @param position   权限组中的位置
     */
    void onDeny(String permission, int position);

    /**
     * 授权成功
     *
     * @param permission 允许的权限
     * @param position   权限组中的位置
     */
    void onGuarantee(String permission, int position);
}
